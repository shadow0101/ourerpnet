﻿Public Class formIndex
    Public Shared _currentID As Integer
    Public form As formUser
    Private UserBLL As ourerpnetBLL.UserBLL
    Private ProductBLL As ourerpnetBLL.ProductBLL
    Private SaleOrderBLL As ourerpnetBLL.SaleOrderBLL
    Private PartnerBLL As ourerpnetBLL.PartnerBLL
    Public currentUser As New ourerpnetModels.UserModel
    Private Sub tileusers_Click(sender As Object, e As EventArgs) Handles tileusers.Click 'USERS TILE
        If Not Application.OpenForms().OfType(Of formUser).Any Then
            form = New formUser
            UserBLL = New ourerpnetBLL.UserBLL
            form.TopLevel = False
            'form.Visible = True
            'form.TopMost = True
            form.bs.DataSource = UserBLL.Users()
            form.Parent = SplitContainer1.Panel2
            SplitContainer1.Panel2.Controls.Add(form)
            form.dgvTreeView.DataSource = form.BS
            form.Show()
        End If
    End Sub
    Private Sub MetroTile1_Click(sender As Object, e As EventArgs) Handles MetroTile1.Click 'PROFILE TILE
        If SplitContainer2.Panel2.Controls.Count > 0 Then
            SplitContainer2.Panel2.Controls(0).Dispose()
            SplitContainer2.Panel2.Controls.Clear()
        End If
        UserBLL = New ourerpnetBLL.UserBLL
        Dim Current_user As ourerpnetModels.UserModel
        Current_user = UserBLL.Browse(_currentID)
        Dim formUser As New formUserFormView
        With formUser
            .TopLevel = False
            .Parent = SplitContainer2.Panel2
            .FormBorderStyle = FormBorderStyle.None
            .lblUserID.Text = Current_user.ID
            .cbxIsadmin.Checked = Current_user.IsAdmin
            .txtName.Text = Current_user.Name
            .txtLogin.Text = Current_user.Login
            .tglActive.Checked = Current_user.Active
            .tglSettings.Checked = Current_user.Settings
        End With
        For Each Control As Control In formUser.Controls
            Control.Enabled = False
        Next
        SplitContainer2.Panel2.Controls.Add(formUser)
        formUser.Show()
    End Sub
    Private Sub formIndex_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MetroTabControl1.SelectedTab = MetroTabPage1
        If currentUser.Settings = False Then
            MetroTabControl1.TabPages.Remove(MetroTabPage2)
        End If
    End Sub
    Private Sub MetroTile3_Click(sender As Object, e As EventArgs) Handles MetroTile3.Click
        Dim loginform As New login_form
        loginform.Show()
        Me.Close()
    End Sub
    Private Sub tileProducts_Click(sender As Object, e As EventArgs) Handles tileProducts.Click 'PRODUCT BUTTON 'TODO
        If SplitContainer3.Panel2.Controls.Count > 0 Then
            SplitContainer3.Panel2.Controls(0).Dispose()
            SplitContainer3.Panel2.Controls.Clear()
        End If
        Dim fp As New formProduct
        ProductBLL = New ourerpnetBLL.ProductBLL
        fp.TopLevel = False
        fp.BS.DataSource = ProductBLL.Products()
        fp.Parent = SplitContainer3.Panel2
        SplitContainer3.Panel2.Controls.Add(fp)
        fp.dgvTreeView.DataSource = fp.BS
        fp.Show()
    End Sub

    Private Sub MetroTile6_Click(sender As Object, e As EventArgs) Handles MetroTile6.Click 'CUSTOMER TILE
        '      If Not Application.OpenForms().OfType(Of formCustomer).Any Then
        If SplitContainer4.Panel2.Controls.Count > 0 Then
            SplitContainer4.Panel2.Controls(0).Dispose()
            SplitContainer4.Panel2.Controls.Clear()
        End If
        Dim fc As New formCustomer
        PartnerBLL = New ourerpnetBLL.PartnerBLL
        fc.TopLevel = False
        fc.BS.DataSource = PartnerBLL.Partners()
        fc.Parent = SplitContainer4.Panel2
        SplitContainer4.Panel2.Controls.Add(fc)
        fc.dgvTreeView.DataSource = fc.BS
        fc.Show()
        '  End If
    End Sub
    Private Sub MetroTile5_Click(sender As Object, e As EventArgs) Handles MetroTile5.Click 'QUOTATION TILE
        Dim fquot As New formQuotation
        'If Not Application.OpenForms().OfType(Of formQuotation).Any Then
        If SplitContainer4.Panel2.Controls.Count > 0 Then
            SplitContainer4.Panel2.Controls(0).Dispose()
            SplitContainer4.Panel2.Controls.Clear()
        End If
        SaleOrderBLL = New ourerpnetBLL.SaleOrderBLL
        fquot.BS.DataSource = SaleOrderBLL.SaleOrders()
        fquot.dgvTreeView.DataSource = fquot.BS
        fquot.TopLevel = False
        fquot.Parent = SplitContainer4.Panel2
        SplitContainer4.Panel2.Controls.Add(fquot)
        fquot.Show()
        '   End If
    End Sub
    Private Sub MetroTile9_Click(sender As Object, e As EventArgs) Handles MetroTile9.Click 'LOCATION TILE
        Dim flot As New formLocation
        'If Not Application.OpenForms().OfType(Of formQuotation).Any Then
        If SplitContainer3.Panel2.Controls.Count > 0 Then
            SplitContainer3.Panel2.Controls(0).Dispose()
            SplitContainer3.Panel2.Controls.Clear()
        End If
        ProductBLL = New ourerpnetBLL.ProductBLL
        flot.BS.DataSource = ProductBLL.Locations()
        flot.dgvTreeView.DataSource = flot.BS
        flot.TopLevel = False
        flot.Parent = SplitContainer3.Panel2
        SplitContainer3.Panel2.Controls.Add(flot)
        flot.Show()
    End Sub

    Private Sub MetroTile11_Click(sender As Object, e As EventArgs) Handles MetroTile11.Click 'HISTORY TILE
        Dim fhis As New formHistory
        If SplitContainer2.Panel2.Controls.Count > 0 Then
            SplitContainer2.Panel2.Controls(0).Dispose()
            SplitContainer2.Panel2.Controls.Clear()
        End If
        UserBLL = New ourerpnetBLL.UserBLL
        fhis.dgvHistory.DataSource = UserBLL.Logs(currentUser)
        fhis.TopLevel = False
        fhis.Parent = SplitContainer2.Panel2
        SplitContainer2.Panel2.Controls.Add(fhis)
        fhis.Show()
    End Sub
    Private Sub MetroTile2_Click(sender As Object, e As EventArgs) Handles MetroTile2.Click
        Dim fsett As New formGeneralSettings
        If SplitContainer1.Panel2.Controls.Count > 0 Then
            SplitContainer1.Panel2.Controls(0).Dispose()
            SplitContainer1.Panel2.Controls.Clear()
        End If
        fsett.TopLevel = False
        fsett.Parent = SplitContainer1.Panel2
        SplitContainer1.Panel2.Controls.Add(fsett)
        fsett.Show()
    End Sub
    Private Sub MetroTile4_Click(sender As Object, e As EventArgs) Handles MetroTile4.Click

    End Sub
End Class