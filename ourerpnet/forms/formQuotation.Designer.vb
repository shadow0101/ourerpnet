﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class formQuotation
    Inherits ourerpnet.formViewForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnExport = New MetroFramework.Controls.MetroButton()
        Me.btnImport = New MetroFramework.Controls.MetroButton()
        CType(Me.formCon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.formCon.Panel1.SuspendLayout()
        Me.formCon.SuspendLayout()
        CType(Me.BS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MetroLabel1
        '
        Me.MetroLabel1.Text = "Quotations"
        '
        'formCon
        '
        '
        'formCon.Panel1
        '
        Me.formCon.Panel1.Controls.Add(Me.btnImport)
        Me.formCon.Panel1.Controls.Add(Me.btnExport)
        '
        'btnExport
        '
        Me.btnExport.Location = New System.Drawing.Point(840, 45)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.Size = New System.Drawing.Size(85, 23)
        Me.btnExport.TabIndex = 23
        Me.btnExport.Text = "Export"
        Me.btnExport.UseSelectable = True
        '
        'btnImport
        '
        Me.btnImport.Location = New System.Drawing.Point(749, 44)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(85, 23)
        Me.btnImport.TabIndex = 24
        Me.btnImport.Text = "Import"
        Me.btnImport.UseSelectable = True
        '
        'formQuotation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1029, 589)
        Me.Name = "formQuotation"
        Me.formCon.Panel1.ResumeLayout(False)
        Me.formCon.Panel1.PerformLayout()
        CType(Me.formCon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.formCon.ResumeLayout(False)
        CType(Me.BS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Protected WithEvents btnExport As MetroFramework.Controls.MetroButton
    Protected WithEvents btnImport As MetroFramework.Controls.MetroButton
End Class
