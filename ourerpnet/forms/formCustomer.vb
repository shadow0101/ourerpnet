﻿Public Class formCustomer
    Inherits formViewForm
    Private _fCustomer As formCustomerFormView
    Private PModel As ourerpnetModels.PartnerModel
    Private CBLL As ourerpnetBLL.PartnerBLL
    Protected Overrides Sub btnDiscard_Click(sender As Object, e As EventArgs)
        MyBase.btnDiscard_Click(sender, e)
        _fCustomer.Close()
    End Sub
    Protected Overrides Sub btnCreate_Click(sender As Object, e As EventArgs)
        If Not Application.OpenForms().OfType(Of formCustomerFormView).Any Then
            MyBase.btnCreate_Click(sender, e)
            Status = "Create"
            _fCustomer = New formCustomerFormView
            With _fCustomer
                .TopLevel = False
                .Parent = formCon.Panel2
            End With
            formCon.Panel2.Controls.Add(_fCustomer)
            _fCustomer.Show()
        End If
    End Sub
    Protected Overrides Sub btnSave_Click(sender As Object, e As EventArgs)
        CBLL = New ourerpnetBLL.PartnerBLL
        If Status = "Create" Then
            PModel = New ourerpnetModels.PartnerModel
            With PModel
                .Name = _fCustomer.txtName.Text
                .Active = _fCustomer.tglActive.Checked
                .Street = _fCustomer.txtStreet.Text
                .City = _fCustomer.txtCity.Text
                .Zip = _fCustomer.txtZip.Text
                .Email = _fCustomer.txtEmail.Text
                .JobPosition = _fCustomer.txtJobPosition.Text
                .Phone = _fCustomer.txtPhone.Text
                .Mobile = _fCustomer.txtMobile.Text
                .Fax = _fCustomer.txtFax.Text
                .CreateUID = formIndex._currentID
                .WriteUID = formIndex._currentID
            End With
            If CBLL.Create(PModel) = True Then

                _fCustomer.Close()
            End If
        ElseIf Status = "Write" Then
            PModel = New ourerpnetModels.PartnerModel
            With PModel
                .Name = _fCustomer.txtName.Text
                .Active = _fCustomer.tglActive.Checked
                .Street = _fCustomer.txtStreet.Text
                .City = _fCustomer.txtCity.Text
                .Zip = _fCustomer.txtZip.Text
                .Email = _fCustomer.txtEmail.Text
                .JobPosition = _fCustomer.txtJobPosition.Text
                .Phone = _fCustomer.txtPhone.Text
                .Mobile = _fCustomer.txtMobile.Text
                .Fax = _fCustomer.txtFax.Text
                .WriteUID = formIndex._currentID
                .ID = _fCustomer.lblID.Text
            End With
            If CBLL.Write(PModel) = True Then
                btnEdit.Visible = True
                For Each Control As Control In _fCustomer.Controls
                    Control.Enabled = False
                Next
            End If
        End If
        MyBase.btnSave_Click(sender, e)
    End Sub
    Protected Overrides Sub formViewForm_Load(sender As Object, e As EventArgs)
        MyBase.formViewForm_Load(sender, e)
    End Sub
    Protected Overrides Sub dgvTreeView_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs)
        MyBase.dgvTreeView_CellDoubleClick(sender, e)
        _fCustomer = New formCustomerFormView
        With _fCustomer
            .TopLevel = False
            .Parent = formCon.Panel2
            .txtName.DataBindings.Add("Text", BS, "Name")
            .tglActive.DataBindings.Add("Checked", BS, "Active")
            .txtStreet.DataBindings.Add("Text", BS, "Street")
            .txtCity.DataBindings.Add("Text", BS, "City")
            .txtZip.DataBindings.Add("Text", BS, "Zip")
            .txtEmail.DataBindings.Add("Text", BS, "Email")
            .txtJobPosition.DataBindings.Add("Text", BS, "JobPosition")
            .txtPhone.DataBindings.Add("Text", BS, "Phone")
            .txtMobile.DataBindings.Add("Text", BS, "Mobile")
            .txtFax.DataBindings.Add("Text", BS, "Fax")
            .lblID.DataBindings.Add("Text", BS, "ID")
        End With
        For Each Control As Control In _fCustomer.Controls
            Control.Enabled = False
        Next
        formCon.Panel2.Controls.Add(_fCustomer)
        _fCustomer.Show()
    End Sub
    Protected Overrides Sub btnEdit_Click(sender As Object, e As EventArgs)
        MyBase.btnEdit_Click(sender, e)
        For Each Control As Control In _fCustomer.Controls
            Control.Enabled = True
        Next
    End Sub
    Protected Overrides Sub DeleteToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Form = "partner"
        MyBase.DeleteToolStripMenuItem_Click(sender, e)
    End Sub
    Protected Overrides Sub Reload()
        CBLL = New ourerpnetBLL.PartnerBLL
        BS.DataSource = CBLL.Partners()
        MyBase.Reload()
    End Sub
End Class