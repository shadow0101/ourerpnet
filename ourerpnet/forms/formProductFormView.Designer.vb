﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class formProductFormView
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtName = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.tglActive = New MetroFramework.Controls.MetroToggle()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.cmbCategory = New MetroFramework.Controls.MetroComboBox()
        Me.MetroLabel7 = New MetroFramework.Controls.MetroLabel()
        Me.txtCost = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel6 = New MetroFramework.Controls.MetroLabel()
        Me.txtSalePrice = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel4 = New MetroFramework.Controls.MetroLabel()
        Me.txtBarcode = New MetroFramework.Controls.MetroTextBox()
        Me.cmbType = New MetroFramework.Controls.MetroComboBox()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.MetroLabel13 = New MetroFramework.Controls.MetroLabel()
        Me.txtQuantity = New MetroFramework.Controls.MetroTextBox()
        Me.cmbLocation = New MetroFramework.Controls.MetroComboBox()
        Me.MetroLabel10 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel9 = New MetroFramework.Controls.MetroLabel()
        Me.txtWeight = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel8 = New MetroFramework.Controls.MetroLabel()
        Me.txtVolume = New MetroFramework.Controls.MetroTextBox()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.cmbTax = New MetroFramework.Controls.MetroComboBox()
        Me.MetroLabel12 = New MetroFramework.Controls.MetroLabel()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.txtDescription = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel11 = New MetroFramework.Controls.MetroLabel()
        Me.lblID = New MetroFramework.Controls.MetroLabel()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtName
        '
        '
        '
        '
        Me.txtName.CustomButton.Image = Nothing
        Me.txtName.CustomButton.Location = New System.Drawing.Point(292, 2)
        Me.txtName.CustomButton.Name = ""
        Me.txtName.CustomButton.Size = New System.Drawing.Size(37, 37)
        Me.txtName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtName.CustomButton.TabIndex = 1
        Me.txtName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtName.CustomButton.UseSelectable = True
        Me.txtName.CustomButton.Visible = False
        Me.txtName.Lines = New String(-1) {}
        Me.txtName.Location = New System.Drawing.Point(204, 63)
        Me.txtName.MaxLength = 32767
        Me.txtName.Multiline = True
        Me.txtName.Name = "txtName"
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtName.SelectedText = ""
        Me.txtName.SelectionLength = 0
        Me.txtName.SelectionStart = 0
        Me.txtName.ShortcutsEnabled = True
        Me.txtName.Size = New System.Drawing.Size(332, 42)
        Me.txtName.TabIndex = 3
        Me.txtName.UseSelectable = True
        Me.txtName.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtName.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(106, 29)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(102, 19)
        Me.MetroLabel1.TabIndex = 2
        Me.MetroLabel1.Text = "Product Name: "
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.Location = New System.Drawing.Point(778, 29)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(51, 19)
        Me.MetroLabel2.TabIndex = 18
        Me.MetroLabel2.Text = "Active: "
        '
        'tglActive
        '
        Me.tglActive.AutoSize = True
        Me.tglActive.Checked = True
        Me.tglActive.CheckState = System.Windows.Forms.CheckState.Checked
        Me.tglActive.Location = New System.Drawing.Point(874, 31)
        Me.tglActive.Name = "tglActive"
        Me.tglActive.Size = New System.Drawing.Size(80, 17)
        Me.tglActive.TabIndex = 17
        Me.tglActive.Text = "On"
        Me.tglActive.UseSelectable = True
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Location = New System.Drawing.Point(23, 182)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(969, 207)
        Me.TabControl1.TabIndex = 19
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.cmbCategory)
        Me.TabPage1.Controls.Add(Me.MetroLabel7)
        Me.TabPage1.Controls.Add(Me.txtCost)
        Me.TabPage1.Controls.Add(Me.MetroLabel6)
        Me.TabPage1.Controls.Add(Me.txtSalePrice)
        Me.TabPage1.Controls.Add(Me.MetroLabel5)
        Me.TabPage1.Controls.Add(Me.MetroLabel4)
        Me.TabPage1.Controls.Add(Me.txtBarcode)
        Me.TabPage1.Controls.Add(Me.cmbType)
        Me.TabPage1.Controls.Add(Me.MetroLabel3)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(961, 181)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "General Information"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'cmbCategory
        '
        Me.cmbCategory.FormattingEnabled = True
        Me.cmbCategory.ItemHeight = 23
        Me.cmbCategory.Location = New System.Drawing.Point(130, 112)
        Me.cmbCategory.Name = "cmbCategory"
        Me.cmbCategory.Size = New System.Drawing.Size(167, 29)
        Me.cmbCategory.TabIndex = 28
        Me.cmbCategory.UseSelectable = True
        '
        'MetroLabel7
        '
        Me.MetroLabel7.AutoSize = True
        Me.MetroLabel7.Location = New System.Drawing.Point(425, 69)
        Me.MetroLabel7.Name = "MetroLabel7"
        Me.MetroLabel7.Size = New System.Drawing.Size(38, 19)
        Me.MetroLabel7.TabIndex = 27
        Me.MetroLabel7.Text = "Cost:"
        '
        'txtCost
        '
        '
        '
        '
        Me.txtCost.CustomButton.Image = Nothing
        Me.txtCost.CustomButton.Location = New System.Drawing.Point(144, 1)
        Me.txtCost.CustomButton.Name = ""
        Me.txtCost.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtCost.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtCost.CustomButton.TabIndex = 1
        Me.txtCost.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtCost.CustomButton.UseSelectable = True
        Me.txtCost.CustomButton.Visible = False
        Me.txtCost.Lines = New String() {"0.00"}
        Me.txtCost.Location = New System.Drawing.Point(534, 69)
        Me.txtCost.MaxLength = 32767
        Me.txtCost.Name = "txtCost"
        Me.txtCost.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCost.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtCost.SelectedText = ""
        Me.txtCost.SelectionLength = 0
        Me.txtCost.SelectionStart = 0
        Me.txtCost.ShortcutsEnabled = True
        Me.txtCost.Size = New System.Drawing.Size(166, 23)
        Me.txtCost.TabIndex = 26
        Me.txtCost.Text = "0.00"
        Me.txtCost.UseSelectable = True
        Me.txtCost.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtCost.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel6
        '
        Me.MetroLabel6.AutoSize = True
        Me.MetroLabel6.Location = New System.Drawing.Point(425, 25)
        Me.MetroLabel6.Name = "MetroLabel6"
        Me.MetroLabel6.Size = New System.Drawing.Size(69, 19)
        Me.MetroLabel6.TabIndex = 25
        Me.MetroLabel6.Text = "Sale Price:"
        '
        'txtSalePrice
        '
        '
        '
        '
        Me.txtSalePrice.CustomButton.Image = Nothing
        Me.txtSalePrice.CustomButton.Location = New System.Drawing.Point(144, 1)
        Me.txtSalePrice.CustomButton.Name = ""
        Me.txtSalePrice.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtSalePrice.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtSalePrice.CustomButton.TabIndex = 1
        Me.txtSalePrice.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtSalePrice.CustomButton.UseSelectable = True
        Me.txtSalePrice.CustomButton.Visible = False
        Me.txtSalePrice.Lines = New String() {"1.00"}
        Me.txtSalePrice.Location = New System.Drawing.Point(534, 25)
        Me.txtSalePrice.MaxLength = 32767
        Me.txtSalePrice.Name = "txtSalePrice"
        Me.txtSalePrice.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSalePrice.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtSalePrice.SelectedText = ""
        Me.txtSalePrice.SelectionLength = 0
        Me.txtSalePrice.SelectionStart = 0
        Me.txtSalePrice.ShortcutsEnabled = True
        Me.txtSalePrice.Size = New System.Drawing.Size(166, 23)
        Me.txtSalePrice.TabIndex = 24
        Me.txtSalePrice.Text = "1.00"
        Me.txtSalePrice.UseSelectable = True
        Me.txtSalePrice.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtSalePrice.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.Location = New System.Drawing.Point(22, 112)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(67, 19)
        Me.MetroLabel5.TabIndex = 23
        Me.MetroLabel5.Text = "Category:"
        '
        'MetroLabel4
        '
        Me.MetroLabel4.AutoSize = True
        Me.MetroLabel4.Location = New System.Drawing.Point(22, 69)
        Me.MetroLabel4.Name = "MetroLabel4"
        Me.MetroLabel4.Size = New System.Drawing.Size(61, 19)
        Me.MetroLabel4.TabIndex = 21
        Me.MetroLabel4.Text = "Barcode:"
        '
        'txtBarcode
        '
        '
        '
        '
        Me.txtBarcode.CustomButton.Image = Nothing
        Me.txtBarcode.CustomButton.Location = New System.Drawing.Point(144, 1)
        Me.txtBarcode.CustomButton.Name = ""
        Me.txtBarcode.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtBarcode.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtBarcode.CustomButton.TabIndex = 1
        Me.txtBarcode.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtBarcode.CustomButton.UseSelectable = True
        Me.txtBarcode.CustomButton.Visible = False
        Me.txtBarcode.Lines = New String(-1) {}
        Me.txtBarcode.Location = New System.Drawing.Point(131, 69)
        Me.txtBarcode.MaxLength = 32767
        Me.txtBarcode.Name = "txtBarcode"
        Me.txtBarcode.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBarcode.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtBarcode.SelectedText = ""
        Me.txtBarcode.SelectionLength = 0
        Me.txtBarcode.SelectionStart = 0
        Me.txtBarcode.ShortcutsEnabled = True
        Me.txtBarcode.Size = New System.Drawing.Size(166, 23)
        Me.txtBarcode.TabIndex = 20
        Me.txtBarcode.UseSelectable = True
        Me.txtBarcode.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtBarcode.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'cmbType
        '
        Me.cmbType.FormattingEnabled = True
        Me.cmbType.ItemHeight = 23
        Me.cmbType.Items.AddRange(New Object() {"{""consume"",Consumable}", "{service,Service}", "{product,Product}"})
        Me.cmbType.Location = New System.Drawing.Point(130, 25)
        Me.cmbType.Name = "cmbType"
        Me.cmbType.Size = New System.Drawing.Size(167, 29)
        Me.cmbType.TabIndex = 6
        Me.cmbType.UseSelectable = True
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.Location = New System.Drawing.Point(22, 25)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(89, 19)
        Me.MetroLabel3.TabIndex = 5
        Me.MetroLabel3.Text = "Product Type:"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.MetroLabel13)
        Me.TabPage2.Controls.Add(Me.txtQuantity)
        Me.TabPage2.Controls.Add(Me.cmbLocation)
        Me.TabPage2.Controls.Add(Me.MetroLabel10)
        Me.TabPage2.Controls.Add(Me.MetroLabel9)
        Me.TabPage2.Controls.Add(Me.txtWeight)
        Me.TabPage2.Controls.Add(Me.MetroLabel8)
        Me.TabPage2.Controls.Add(Me.txtVolume)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(961, 181)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Inventory"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'MetroLabel13
        '
        Me.MetroLabel13.AutoSize = True
        Me.MetroLabel13.Location = New System.Drawing.Point(41, 94)
        Me.MetroLabel13.Name = "MetroLabel13"
        Me.MetroLabel13.Size = New System.Drawing.Size(61, 19)
        Me.MetroLabel13.TabIndex = 32
        Me.MetroLabel13.Text = "Quantity:"
        '
        'txtQuantity
        '
        '
        '
        '
        Me.txtQuantity.CustomButton.Image = Nothing
        Me.txtQuantity.CustomButton.Location = New System.Drawing.Point(144, 1)
        Me.txtQuantity.CustomButton.Name = ""
        Me.txtQuantity.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtQuantity.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtQuantity.CustomButton.TabIndex = 1
        Me.txtQuantity.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtQuantity.CustomButton.UseSelectable = True
        Me.txtQuantity.CustomButton.Visible = False
        Me.txtQuantity.Lines = New String() {"0"}
        Me.txtQuantity.Location = New System.Drawing.Point(141, 94)
        Me.txtQuantity.MaxLength = 32767
        Me.txtQuantity.Name = "txtQuantity"
        Me.txtQuantity.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtQuantity.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtQuantity.SelectedText = ""
        Me.txtQuantity.SelectionLength = 0
        Me.txtQuantity.SelectionStart = 0
        Me.txtQuantity.ShortcutsEnabled = True
        Me.txtQuantity.Size = New System.Drawing.Size(166, 23)
        Me.txtQuantity.TabIndex = 31
        Me.txtQuantity.Text = "0"
        Me.txtQuantity.UseSelectable = True
        Me.txtQuantity.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtQuantity.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'cmbLocation
        '
        Me.cmbLocation.FormattingEnabled = True
        Me.cmbLocation.ItemHeight = 23
        Me.cmbLocation.Location = New System.Drawing.Point(141, 36)
        Me.cmbLocation.Name = "cmbLocation"
        Me.cmbLocation.Size = New System.Drawing.Size(167, 29)
        Me.cmbLocation.TabIndex = 30
        Me.cmbLocation.UseSelectable = True
        '
        'MetroLabel10
        '
        Me.MetroLabel10.AutoSize = True
        Me.MetroLabel10.Location = New System.Drawing.Point(41, 42)
        Me.MetroLabel10.Name = "MetroLabel10"
        Me.MetroLabel10.Size = New System.Drawing.Size(61, 19)
        Me.MetroLabel10.TabIndex = 29
        Me.MetroLabel10.Text = "Location:"
        '
        'MetroLabel9
        '
        Me.MetroLabel9.AutoSize = True
        Me.MetroLabel9.Location = New System.Drawing.Point(412, 42)
        Me.MetroLabel9.Name = "MetroLabel9"
        Me.MetroLabel9.Size = New System.Drawing.Size(53, 19)
        Me.MetroLabel9.TabIndex = 27
        Me.MetroLabel9.Text = "Weight:"
        '
        'txtWeight
        '
        '
        '
        '
        Me.txtWeight.CustomButton.Image = Nothing
        Me.txtWeight.CustomButton.Location = New System.Drawing.Point(144, 1)
        Me.txtWeight.CustomButton.Name = ""
        Me.txtWeight.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtWeight.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtWeight.CustomButton.TabIndex = 1
        Me.txtWeight.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtWeight.CustomButton.UseSelectable = True
        Me.txtWeight.CustomButton.Visible = False
        Me.txtWeight.Lines = New String() {"0.00"}
        Me.txtWeight.Location = New System.Drawing.Point(512, 42)
        Me.txtWeight.MaxLength = 32767
        Me.txtWeight.Name = "txtWeight"
        Me.txtWeight.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtWeight.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtWeight.SelectedText = ""
        Me.txtWeight.SelectionLength = 0
        Me.txtWeight.SelectionStart = 0
        Me.txtWeight.ShortcutsEnabled = True
        Me.txtWeight.Size = New System.Drawing.Size(166, 23)
        Me.txtWeight.TabIndex = 26
        Me.txtWeight.Text = "0.00"
        Me.txtWeight.UseSelectable = True
        Me.txtWeight.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtWeight.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel8
        '
        Me.MetroLabel8.AutoSize = True
        Me.MetroLabel8.Location = New System.Drawing.Point(412, 94)
        Me.MetroLabel8.Name = "MetroLabel8"
        Me.MetroLabel8.Size = New System.Drawing.Size(56, 19)
        Me.MetroLabel8.TabIndex = 25
        Me.MetroLabel8.Text = "Volume:"
        '
        'txtVolume
        '
        '
        '
        '
        Me.txtVolume.CustomButton.Image = Nothing
        Me.txtVolume.CustomButton.Location = New System.Drawing.Point(144, 1)
        Me.txtVolume.CustomButton.Name = ""
        Me.txtVolume.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtVolume.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtVolume.CustomButton.TabIndex = 1
        Me.txtVolume.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtVolume.CustomButton.UseSelectable = True
        Me.txtVolume.CustomButton.Visible = False
        Me.txtVolume.Lines = New String() {"0.00"}
        Me.txtVolume.Location = New System.Drawing.Point(512, 94)
        Me.txtVolume.MaxLength = 32767
        Me.txtVolume.Name = "txtVolume"
        Me.txtVolume.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtVolume.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtVolume.SelectedText = ""
        Me.txtVolume.SelectionLength = 0
        Me.txtVolume.SelectionStart = 0
        Me.txtVolume.ShortcutsEnabled = True
        Me.txtVolume.Size = New System.Drawing.Size(166, 23)
        Me.txtVolume.TabIndex = 24
        Me.txtVolume.Text = "0.00"
        Me.txtVolume.UseSelectable = True
        Me.txtVolume.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtVolume.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.cmbTax)
        Me.TabPage4.Controls.Add(Me.MetroLabel12)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(961, 181)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Invoicing"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'cmbTax
        '
        Me.cmbTax.FormattingEnabled = True
        Me.cmbTax.ItemHeight = 23
        Me.cmbTax.Items.AddRange(New Object() {"{""consume"",Consumable}", "{service,Service}", "{product,Product}"})
        Me.cmbTax.Location = New System.Drawing.Point(135, 37)
        Me.cmbTax.Name = "cmbTax"
        Me.cmbTax.Size = New System.Drawing.Size(167, 29)
        Me.cmbTax.TabIndex = 8
        Me.cmbTax.UseSelectable = True
        '
        'MetroLabel12
        '
        Me.MetroLabel12.AutoSize = True
        Me.MetroLabel12.Location = New System.Drawing.Point(60, 47)
        Me.MetroLabel12.Name = "MetroLabel12"
        Me.MetroLabel12.Size = New System.Drawing.Size(30, 19)
        Me.MetroLabel12.TabIndex = 7
        Me.MetroLabel12.Text = "Tax:"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.txtDescription)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(961, 181)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Description"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'txtDescription
        '
        '
        '
        '
        Me.txtDescription.CustomButton.Image = Nothing
        Me.txtDescription.CustomButton.Location = New System.Drawing.Point(781, 1)
        Me.txtDescription.CustomButton.Name = ""
        Me.txtDescription.CustomButton.Size = New System.Drawing.Size(167, 167)
        Me.txtDescription.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtDescription.CustomButton.TabIndex = 1
        Me.txtDescription.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtDescription.CustomButton.UseSelectable = True
        Me.txtDescription.CustomButton.Visible = False
        Me.txtDescription.Lines = New String(-1) {}
        Me.txtDescription.Location = New System.Drawing.Point(6, 6)
        Me.txtDescription.MaxLength = 32767
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtDescription.SelectedText = ""
        Me.txtDescription.SelectionLength = 0
        Me.txtDescription.SelectionStart = 0
        Me.txtDescription.ShortcutsEnabled = True
        Me.txtDescription.Size = New System.Drawing.Size(949, 169)
        Me.txtDescription.TabIndex = 4
        Me.txtDescription.UseSelectable = True
        Me.txtDescription.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtDescription.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel11
        '
        Me.MetroLabel11.AutoSize = True
        Me.MetroLabel11.Location = New System.Drawing.Point(820, 151)
        Me.MetroLabel11.Name = "MetroLabel11"
        Me.MetroLabel11.Size = New System.Drawing.Size(74, 19)
        Me.MetroLabel11.TabIndex = 20
        Me.MetroLabel11.Text = "Product ID:"
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(910, 151)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(0, 0)
        Me.lblID.TabIndex = 21
        '
        'formProductFormView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1025, 502)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblID)
        Me.Controls.Add(Me.MetroLabel11)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.MetroLabel2)
        Me.Controls.Add(Me.tglActive)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.MetroLabel1)
        Me.Name = "formProductFormView"
        Me.ShadowType = MetroFramework.Forms.MetroFormShadowType.None
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Image"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtName As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents tglActive As MetroFramework.Controls.MetroToggle
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents cmbType As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel7 As MetroFramework.Controls.MetroLabel
    Friend WithEvents txtCost As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel6 As MetroFramework.Controls.MetroLabel
    Friend WithEvents txtSalePrice As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel4 As MetroFramework.Controls.MetroLabel
    Friend WithEvents txtBarcode As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel9 As MetroFramework.Controls.MetroLabel
    Friend WithEvents txtWeight As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel8 As MetroFramework.Controls.MetroLabel
    Friend WithEvents txtVolume As MetroFramework.Controls.MetroTextBox
    Friend WithEvents cmbLocation As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel10 As MetroFramework.Controls.MetroLabel
    Friend WithEvents cmbCategory As MetroFramework.Controls.MetroComboBox
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents txtDescription As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel11 As MetroFramework.Controls.MetroLabel
    Friend WithEvents lblID As MetroFramework.Controls.MetroLabel
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents cmbTax As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel12 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel13 As MetroFramework.Controls.MetroLabel
    Friend WithEvents txtQuantity As MetroFramework.Controls.MetroTextBox
End Class
