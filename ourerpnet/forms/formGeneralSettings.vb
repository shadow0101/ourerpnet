﻿Imports System.Security.AccessControl
Public Class formGeneralSettings
    Private BBLL As New ourerpnetBLL.BaseBLL
    Private Sub MetroButton1_Click(sender As Object, e As EventArgs) Handles MetroButton1.Click
        Try
            If (FolderBrowserDialog1.ShowDialog = Windows.Forms.DialogResult.OK) Then
                txtPath.Text = FolderBrowserDialog1.SelectedPath
                Dim BModel As New ourerpnetModels.SettingModel
                'Dim FolderPath As String = FolderBrowserDialog1.SelectedPath
                'Dim UserAccount As String = BModel.Windows
                'Dim FolderInfo As IO.DirectoryInfo = New IO.DirectoryInfo(FolderPath)
                'Dim FolderAcl As DirectorySecurity = BBLL.Access(UserAccount)
                'FolderInfo.SetAccessControl(FolderAcl)
                If BBLL.UpdatePath(FolderBrowserDialog1.SelectedPath) = True Then
                    MessageBox.Show("Path updated")
                    reload()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub formGeneralSettings_Load(sender As Object, e As EventArgs) Handles Me.Load
        reload()
    End Sub
    Public Sub reload()
        txtPath.Text = BBLL.Settings.Path
    End Sub
End Class