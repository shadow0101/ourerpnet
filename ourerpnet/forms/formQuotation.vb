﻿Public Class formQuotation
    Inherits formViewForm
    Private fquot As formQuotationFormView
    Private SOModel As ourerpnetModels.SaleOrderModel
    Private SOBLL As ourerpnetBLL.SaleOrderBLL
    Protected Overrides Sub btnDiscard_Click(sender As Object, e As EventArgs)
        MyBase.btnDiscard_Click(sender, e)
        fquot.Close()
    End Sub
    Protected Overrides Sub btnCreate_Click(sender As Object, e As EventArgs)
        If Not Application.OpenForms().OfType(Of formProductFormView).Any Then
            MyBase.btnCreate_Click(sender, e)
            Status = "Create"
            fquot = New formQuotationFormView
            With fquot
                .TopLevel = False
                .Parent = formCon.Panel2
            End With
            formCon.Panel2.Controls.Add(fquot)
            fquot.Show()
        End If
    End Sub
    Protected Overrides Sub btnSave_Click(sender As Object, e As EventArgs)
        'SOBLL = New ourerpnetBLL.SaleOrderBLL
        If Status = "Create" Then
            'SOModel = New ourerpnetModels.SaleOrderModel
            'With SOModel
            'End With
            fquot.SOBLL.PartnerID(fquot.cmbPartner.SelectedValue)
            If fquot.SOBLL.Create(formIndex._currentID) = True Then
                fquot.Close()
            End If
            'ElseIf Status = "Write" Then
            '    SOModel = New ourerpnetModels.SaleOrderModel
            '    With SOModel
            '        .PartnerID = fquot.cmbPartner.SelectedValue
            '        .DateOrder = fquot.dtpOrderDate.Value
            '    End With
            '    If SOBLL.Write(SOModel) = True Then
            '        btnEdit.Visible = True
            '        For Each Control As Control In fquot.Controls
            '            Control.Enabled = False
            '        Next
            '    End If
        End If
        MyBase.btnSave_Click(sender, e)
    End Sub
    Protected Overrides Sub formViewForm_Load(sender As Object, e As EventArgs)
        MyBase.formViewForm_Load(sender, e)
    End Sub
    Protected Overrides Sub dgvTreeView_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs)
        MyBase.dgvTreeView_CellDoubleClick(sender, e)
        fquot = New formQuotationFormView
        With fquot
            .TopLevel = False
            .Parent = formCon.Panel2
            .lblName.DataBindings.Add("Text", BS, "Name")
            .cmbPartner.DataBindings.Add("SelectedValue", BS, "PartnerID")
            .dtpOrderDate.DataBindings.Add("Value", BS, "OrderDate")
            .dtpExpirationDate.DataBindings.Add("Value", BS, "ExpirationDate")
            '.txtVolume.DataBindings.Add("Text", BS, "Volume")
            '.txtDescription.DataBindings.Add("Text", BS, "Description")
            '.lblID.DataBindings.Add("Text", BS, "ID")
        End With
        For Each Control As Control In fquot.Controls
            Control.Enabled = False
        Next
        formCon.Panel2.Controls.Add(fquot)
        fquot.Show()
    End Sub
    Protected Overrides Sub btnEdit_Click(sender As Object, e As EventArgs)
        MyBase.btnEdit_Click(sender, e)
        For Each Control As Control In fquot.Controls
            Control.Enabled = True
        Next
    End Sub
    Protected Overrides Sub DeleteToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Form = "sale_order"
        MyBase.DeleteToolStripMenuItem_Click(sender, e)
    End Sub
    Protected Overrides Sub Reload()
        SOBLL = New ourerpnetBLL.SaleOrderBLL
        BS.DataSource = SOBLL.SaleOrders()
        MyBase.Reload()
    End Sub
    Private Sub btnImport_Click(sender As Object, e As EventArgs) Handles btnImport.Click
        SOBLL = New ourerpnetBLL.SaleOrderBLL
        Dim BBLL As New ourerpnetBLL.BaseBLL
        SOBLL.BackupSaleOrders()
        MessageBox.Show("Imported to " & BBLL.Settings.Path)
    End Sub
End Class