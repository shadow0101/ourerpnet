﻿Public Class login_form
    Private Sub login()
        Try
            Dim Login As String = txtLogin.Text
            Dim Password As String = txtPassword.Text
            Dim UserBLL As New ourerpnetBLL.UserBLL
            Dim user As ourerpnetModels.UserModel = UserBLL.LoginUser(Login, Password)
            If Not IsNothing(user.Login) Then
                Dim varFormIndex As New formIndex

                formIndex._currentID = user.ID
                varFormIndex.currentUser = UserBLL.Browse(user.ID)
                varFormIndex.Show()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click
        login()
    End Sub
    Private Sub txtLogin_KeyDown(sender As Object, e As KeyEventArgs) Handles txtLogin.KeyDown, txtPassword.KeyDown
        If e.KeyCode = Keys.Enter Then
            login()
        End If
    End Sub
End Class