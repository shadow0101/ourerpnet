﻿Public Class formUser
    Inherits formViewForm
    Public _formuserview As formUserFormView
    Private UModel As ourerpnetModels.UserModel
    Private UBLL As ourerpnetBLL.UserBLL
    Protected Overrides Sub btnDiscard_Click(sender As Object, e As EventArgs)
        MyBase.btnDiscard_Click(sender, e)
        _formuserview.Close()
    End Sub
    Protected Overrides Sub btnCreate_Click(sender As Object, e As EventArgs)
        If Not Application.OpenForms().OfType(Of formProductFormView).Any Then
            MyBase.btnCreate_Click(sender, e)
            Status = "Create"
            _formuserview = New formUserFormView
            With _formuserview
                .TopLevel = False
                .Parent = formCon.Panel2
            End With
            formCon.Panel2.Controls.Add(_formuserview)
            _formuserview.Show()
        End If
    End Sub
    Protected Overrides Sub btnSave_Click(sender As Object, e As EventArgs)
        UBLL = New ourerpnetBLL.UserBLL
        If Status = "Create" Then
            UModel = New ourerpnetModels.UserModel
            With UModel
                .Name = _formuserview.txtName.Text
                .Login = _formuserview.txtLogin.Text
                .Active = _formuserview.tglActive.Checked
                .Settings = _formuserview.tglSettings.Checked
                .IsAdmin = _formuserview.cbxIsadmin.Checked
                .CreateUID = formIndex._currentID
                .WriteUID = formIndex._currentID
            End With
            If UBLL.Create(UModel) = True Then
                _formuserview.Close()
            End If
        ElseIf Status = "Write" Then
            UModel = New ourerpnetModels.UserModel
            With UModel
                .ID = _formuserview.User.ID
                .Name = _formuserview.txtName.Text
                .Login = _formuserview.txtLogin.Text
                .Active = _formuserview.tglActive.Checked
                .Settings = _formuserview.tglSettings.Checked
                .IsAdmin = _formuserview.cbxIsadmin.Checked
                .WriteUID = formIndex._currentID
                .PasswordCrypt = _formuserview.User.PasswordCrypt
            End With
            'If UBLL.Write(UModel) = True Then
            ' btnEdit.Visible = True
            'For Each Control As Control In _formuserview.Controls
            ' Control.Enabled = False
            ' Next
            ' Else
            '    MessageBox.Show("awef")
            'End If
        End If
        MyBase.btnSave_Click(sender, e)
    End Sub
    Protected Overrides Sub formViewForm_Load(sender As Object, e As EventArgs)
        MyBase.formViewForm_Load(sender, e)
    End Sub
    Protected Overrides Sub dgvTreeView_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs)
        MyBase.dgvTreeView_CellDoubleClick(sender, e)
        _formuserview = New formUserFormView
        With _formuserview
            .TopLevel = False
            .Parent = formCon.Panel2
            .txtName.DataBindings.Add("Text", BS, "Name")
            .cbxIsadmin.DataBindings.Add("Checked", BS, "IsAdmin")
            .txtLogin.DataBindings.Add("Text", BS, "Login")
            .tglActive.DataBindings.Add("Checked", BS, "Active")
            .tglSettings.DataBindings.Add("Checked", BS, "Settings")
            .lblUserID.DataBindings.Add("Text", BS, "ID")
        End With
        For Each Control As Control In _formuserview.Controls
            Control.Enabled = False
        Next
        formCon.Panel2.Controls.Add(_formuserview)
        _formuserview.Show()
    End Sub
    Protected Overrides Sub btnEdit_Click(sender As Object, e As EventArgs)
        MyBase.btnEdit_Click(sender, e)
        For Each Control As Control In _formuserview.Controls
            Control.Enabled = True
        Next
    End Sub
    Protected Overrides Sub DeleteToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Form = "users"
        MyBase.DeleteToolStripMenuItem_Click(sender, e)
    End Sub
    Protected Overrides Sub Reload()
        UBLL = New ourerpnetBLL.UserBLL
        BS.DataSource = UBLL.Users()
        MyBase.Reload()
    End Sub
    End Class
