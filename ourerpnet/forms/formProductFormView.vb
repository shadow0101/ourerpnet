﻿Public Class formProductFormView
    Private Sub formProductFormView_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim p As New ourerpnetModels.ProductModel
        Dim PBLL As New ourerpnetBLL.ProductBLL
        With cmbType
            .DataSource = New BindingSource(p.TypeList, Nothing)
            .DisplayMember = "Value"
            .ValueMember = "Key"
        End With
        With cmbCategory
            .DataSource = New BindingSource(PBLL.getCategories(), Nothing)
            .DisplayMember = "Value"
            .ValueMember = "Key"
        End With
        With cmbLocation
            .DataSource = New BindingSource(PBLL.getLocations(), Nothing)
            .DisplayMember = "Value"
            .ValueMember = "Key"
        End With
        With cmbTax
            .DataSource = New BindingSource(PBLL.getTaxes(), Nothing)
            .DisplayMember = "Value"
            .ValueMember = "Key"
        End With

    End Sub
End Class