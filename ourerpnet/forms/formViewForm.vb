﻿Public Class formViewForm
    Public BS As New BindingSource
    Protected Status As String
    Protected Form As String
    Private BBLL As New ourerpnetBLL.BaseBLL
    Protected Overridable Sub btnCreate_Click(sender As Object, e As EventArgs) Handles btnCreate.Click
        btnSave.Visible = True
        btnDiscard.Visible = True
        btnCreate.Visible = False
        dgvTreeView.Visible = False
    End Sub
    Protected Overridable Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        btnCreate.Visible = False
        btnSave.Visible = False
        btnDiscard.Visible = False
        If Status = "Create" Then
            Me.Reload()
            btnCreate.Visible = True
            dgvTreeView.Show()
        End If
    End Sub
    Protected Overridable Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Status = "Write"
        btnSave.Visible = True
        btnDiscard.Visible = True
        btnEdit.Visible = False
        btnCreate.Visible = False
    End Sub
    Protected Overridable Sub btnDiscard_Click(sender As Object, e As EventArgs) Handles btnDiscard.Click
        btnCreate.Visible = True
        btnSave.Visible = False
        btnDiscard.Visible = False
        dgvTreeView.Visible = True
    End Sub
    Protected Sub MetroButton2_Click(sender As Object, e As EventArgs) Handles MetroButton2.Click
        BS.MovePrevious()
    End Sub
    Protected Sub MetroButton1_Click(sender As Object, e As EventArgs) Handles MetroButton1.Click
        BS.MoveNext()
    End Sub
    Protected Overridable Sub formViewForm_Load(sender As Object, e As EventArgs) Handles Me.Load
        If (Me.DesignMode) Then
            Return
        End If
        MyClass.Reload()
    End Sub
    Protected Overridable Sub dgvTreeView_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvTreeView.CellDoubleClick
        Dim id As Integer = dgvTreeView.CurrentRow.Cells("ID").Value
        dgvTreeView.Visible = False
        btnEdit.Visible = True
        btnCreate.Visible = False
    End Sub
    Protected Overridable Sub DeleteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DeleteToolStripMenuItem.Click
        For Each srows As DataGridViewRow In dgvTreeView.SelectedRows
            BBLL.Delete(Form, srows.Cells("ID").Value)
        Next
        Me.Reload()
    End Sub
    Protected Overridable Sub Reload()
        dgvTreeView.DataSource = Nothing
        dgvTreeView.DataSource = BS
        With dgvTreeView
            .Columns("ID").Visible = False
            .Columns("Active").Visible = False
            .Columns("WriteUID").Visible = False
            .Columns("CreateUID").Visible = False
            .Columns("Name").DisplayIndex = 0
        End With
    End Sub
End Class
