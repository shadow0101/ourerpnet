﻿Imports ourerpnetBLL
Imports ourerpnetModels
Public Class formQuotationFormView
    Private PBLL As New ProductBLL
    Private CBLL As New PartnerBLL
    Public SOBLL As New SaleOrderBLL
    Private OLModel As OrderLineModel
    Private Sub formQuotationFormView_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim cbo = CType(MetroGrid1.Columns("colProduct"), DataGridViewComboBoxColumn)
        cbo.DataSource = PBLL.Products()
        cbo.ValueMember = "ID"
        cbo.DisplayMember = "Name"
        cmbPartner.DataSource = CBLL.Partners()
        cmbPartner.DisplayMember = "Name"
        cmbPartner.ValueMember = "ID"
    End Sub
    Private Sub MetroGrid1_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles MetroGrid1.CellEndEdit
        Dim row As Integer = MetroGrid1.CurrentCell.RowIndex
        Dim id As Integer = MetroGrid1.Rows(row).Cells("colProductID").Value
        Dim qty As Integer = MetroGrid1.Rows(row).Cells("colQuantity").Value
        Try
            If e.ColumnIndex = 3 Then
                PBLL.CheckQuantity(id, qty)
                SOBLL.Update(id, qty)
                MetroGrid1.Rows(row).Cells("colSubtotal").Value = OLModel.PriceSubtotal
                lblTaxes.Text = SOBLL.Taxes()
                lblUntaxed.Text = SOBLL.UntaxedAmount()
                lblTotal.Text = SOBLL.Total()
            End If
        Catch ex As Exception
            MetroGrid1.Rows(row).Cells("colQuantity").Value = SOBLL.LastQuantity(id)
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub MetroGrid1_EditingControlShowing(sender As Object, e As DataGridViewEditingControlShowingEventArgs) Handles MetroGrid1.EditingControlShowing
        If MetroGrid1.CurrentCell.ColumnIndex = 0 Then
            Dim cmb As ComboBox = CType(e.Control, ComboBox)
            If (cmb IsNot Nothing) Then
                RemoveHandler cmb.SelectionChangeCommitted, New EventHandler(AddressOf ComboBox_SelectionChangeCommitted)
                AddHandler cmb.SelectionChangeCommitted, New EventHandler(AddressOf ComboBox_SelectionChangeCommitted)
            End If
        End If
    End Sub
    Private Sub ComboBox_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)
            Dim id As Integer = cmb.SelectedValue
            Dim row As Integer = MetroGrid1.CurrentCell.RowIndex
            Dim pinfo As ourerpnetModels.ProductModel = PBLL.Browse(id)
            With MetroGrid1
                .Rows(row).Cells("colProductID").Value = pinfo.ID
                .Rows(row).Cells("colDescription").Value = pinfo.Description
                .Rows(row).Cells("colPrice").Value = pinfo.SalePrice
                .Rows(row).Cells("colQuantity").Value = 1
                .Rows(row).Cells("colTax").Value = pinfo.TaxName
                .Rows(row).Cells("colTaxID").Value = pinfo.TaxID
                .Rows(row).Cells("colTaxAmount").Value = pinfo.TaxAmount
            End With
            OLModel = New OrderLineModel(pinfo.ID, pinfo.SalePrice, 1, pinfo.TaxAmount)
            SOBLL.Add(OLModel)
            MetroGrid1.Rows(row).Cells("colSubtotal").Value = OLModel.PriceSubtotal

            lblTaxes.Text = SOBLL.Taxes()
            lblUntaxed.Text = SOBLL.UntaxedAmount()
            lblTotal.Text = SOBLL.Total()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class