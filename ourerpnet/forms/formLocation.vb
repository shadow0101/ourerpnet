﻿Public Class formLocation
    Inherits formViewForm
    Private _fLocation As formLocationFormView
    Private PModel As ourerpnetModels.ProductModel
    Private PBLL As ourerpnetBLL.ProductBLL
    Protected Overrides Sub btnDiscard_Click(sender As Object, e As EventArgs)
        MyBase.btnDiscard_Click(sender, e)
        _fLocation.Close()
    End Sub
    Protected Overrides Sub btnCreate_Click(sender As Object, e As EventArgs)
        If Not Application.OpenForms().OfType(Of formProductFormView).Any Then
            MyBase.btnCreate_Click(sender, e)
            Status = "Create"
            _fLocation = New formLocationFormView
            With _fLocation
                .TopLevel = False
                .Parent = formCon.Panel2
            End With
            formCon.Panel2.Controls.Add(_fLocation)
            _fLocation.Show()
        End If
    End Sub
    Protected Overrides Sub btnSave_Click(sender As Object, e As EventArgs)
        PBLL = New ourerpnetBLL.ProductBLL
        If Status = "Create" Then
            PModel = New ourerpnetModels.ProductModel
            With PModel
                .Name = _fLocation.txtName.Text
                .Active = _fLocation.tglActive.Checked
                .CreateUID = formIndex._currentID
                .WriteUID = formIndex._currentID
            End With
            If PBLL.Create(PModel) = True Then
                _fLocation.Close()
            End If
        ElseIf Status = "Write" Then
            PModel = New ourerpnetModels.ProductModel
            With PModel
                .Name = _fLocation.txtName.Text
                .Active = _fLocation.tglActive.Checked
                .WriteUID = formIndex._currentID

            End With
            If PBLL.Write(PModel) = True Then
                btnEdit.Visible = True
                For Each Control As Control In _fLocation.Controls
                    Control.Enabled = False
                Next
            Else
                MessageBox.Show("awef")
            End If
        End If
        MyBase.btnSave_Click(sender, e)
    End Sub
    Protected Overrides Sub formViewForm_Load(sender As Object, e As EventArgs)
        MyBase.formViewForm_Load(sender, e)
    End Sub
    Protected Overrides Sub dgvTreeView_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs)
        MyBase.dgvTreeView_CellDoubleClick(sender, e)
        _fLocation = New formLocationFormView
        With _fLocation
            .TopLevel = False
            .Parent = formCon.Panel2
            .txtName.DataBindings.Add("Text", BS, "Name")
            .tglActive.DataBindings.Add("Checked", BS, "Active")
        
        End With
        For Each Control As Control In _fLocation.Controls
            Control.Enabled = False
        Next
        formCon.Panel2.Controls.Add(_fLocation)
        _fLocation.Show()
    End Sub
    Protected Overrides Sub btnEdit_Click(sender As Object, e As EventArgs)
        MyBase.btnEdit_Click(sender, e)
        For Each Control As Control In _fLocation.Controls
            Control.Enabled = True
        Next
    End Sub
    Protected Overrides Sub DeleteToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Form = "stock_location"
        MyBase.DeleteToolStripMenuItem_Click(sender, e)
    End Sub
    Protected Overrides Sub Reload()
        PBLL = New ourerpnetBLL.ProductBLL
        BS.DataSource = PBLL.getLocations()
        MyBase.Reload()
    End Sub
End Class