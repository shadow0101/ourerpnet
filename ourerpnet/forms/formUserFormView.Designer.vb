﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class formUserFormView
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.lblUserID = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.txtName = New MetroFramework.Controls.MetroTextBox()
        Me.txtLogin = New MetroFramework.Controls.MetroTextBox()
        Me.tglActive = New MetroFramework.Controls.MetroToggle()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cbxIsadmin = New MetroFramework.Controls.MetroCheckBox()
        Me.MetroLabel7 = New MetroFramework.Controls.MetroLabel()
        Me.tglSettings = New MetroFramework.Controls.MetroToggle()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.Location = New System.Drawing.Point(228, 60)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(52, 19)
        Me.MetroLabel5.TabIndex = 10
        Me.MetroLabel5.Text = "Name: "
        '
        'lblUserID
        '
        Me.lblUserID.AutoSize = True
        Me.lblUserID.Location = New System.Drawing.Point(878, 235)
        Me.lblUserID.Name = "lblUserID"
        Me.lblUserID.Size = New System.Drawing.Size(0, 0)
        Me.lblUserID.TabIndex = 9
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.Location = New System.Drawing.Point(818, 235)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(54, 19)
        Me.MetroLabel3.TabIndex = 8
        Me.MetroLabel3.Text = "User ID:"
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(228, 144)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(44, 19)
        Me.MetroLabel1.TabIndex = 6
        Me.MetroLabel1.Text = "Login:"
        '
        'txtName
        '
        '
        '
        '
        Me.txtName.CustomButton.Image = Nothing
        Me.txtName.CustomButton.Location = New System.Drawing.Point(236, 1)
        Me.txtName.CustomButton.Name = ""
        Me.txtName.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtName.CustomButton.TabIndex = 1
        Me.txtName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtName.CustomButton.UseSelectable = True
        Me.txtName.CustomButton.Visible = False
        Me.txtName.Lines = New String(-1) {}
        Me.txtName.Location = New System.Drawing.Point(269, 97)
        Me.txtName.MaxLength = 32767
        Me.txtName.Name = "txtName"
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtName.SelectedText = ""
        Me.txtName.SelectionLength = 0
        Me.txtName.SelectionStart = 0
        Me.txtName.ShortcutsEnabled = True
        Me.txtName.Size = New System.Drawing.Size(258, 23)
        Me.txtName.TabIndex = 12
        Me.txtName.UseSelectable = True
        Me.txtName.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtName.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'txtLogin
        '
        '
        '
        '
        Me.txtLogin.CustomButton.Image = Nothing
        Me.txtLogin.CustomButton.Location = New System.Drawing.Point(236, 1)
        Me.txtLogin.CustomButton.Name = ""
        Me.txtLogin.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtLogin.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtLogin.CustomButton.TabIndex = 1
        Me.txtLogin.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtLogin.CustomButton.UseSelectable = True
        Me.txtLogin.CustomButton.Visible = False
        Me.txtLogin.Lines = New String(-1) {}
        Me.txtLogin.Location = New System.Drawing.Point(269, 172)
        Me.txtLogin.MaxLength = 32767
        Me.txtLogin.Name = "txtLogin"
        Me.txtLogin.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtLogin.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtLogin.SelectedText = ""
        Me.txtLogin.SelectionLength = 0
        Me.txtLogin.SelectionStart = 0
        Me.txtLogin.ShortcutsEnabled = True
        Me.txtLogin.Size = New System.Drawing.Size(258, 23)
        Me.txtLogin.TabIndex = 14
        Me.txtLogin.UseSelectable = True
        Me.txtLogin.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtLogin.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'tglActive
        '
        Me.tglActive.AutoSize = True
        Me.tglActive.Checked = True
        Me.tglActive.CheckState = System.Windows.Forms.CheckState.Checked
        Me.tglActive.Location = New System.Drawing.Point(881, 36)
        Me.tglActive.Name = "tglActive"
        Me.tglActive.Size = New System.Drawing.Size(80, 17)
        Me.tglActive.TabIndex = 15
        Me.tglActive.Text = "On"
        Me.tglActive.UseSelectable = True
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.Location = New System.Drawing.Point(792, 36)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(51, 19)
        Me.MetroLabel2.TabIndex = 16
        Me.MetroLabel2.Text = "Active: "
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cbxIsadmin)
        Me.GroupBox1.Controls.Add(Me.MetroLabel7)
        Me.GroupBox1.Controls.Add(Me.tglSettings)
        Me.GroupBox1.Location = New System.Drawing.Point(166, 286)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(706, 182)
        Me.GroupBox1.TabIndex = 17
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Access Rights"
        '
        'cbxIsadmin
        '
        Me.cbxIsadmin.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cbxIsadmin.Location = New System.Drawing.Point(32, 53)
        Me.cbxIsadmin.Name = "cbxIsadmin"
        Me.cbxIsadmin.Size = New System.Drawing.Size(126, 31)
        Me.cbxIsadmin.TabIndex = 22
        Me.cbxIsadmin.Text = "Is admin:"
        Me.cbxIsadmin.UseSelectable = True
        '
        'MetroLabel7
        '
        Me.MetroLabel7.AutoSize = True
        Me.MetroLabel7.Location = New System.Drawing.Point(32, 108)
        Me.MetroLabel7.Name = "MetroLabel7"
        Me.MetroLabel7.Size = New System.Drawing.Size(57, 19)
        Me.MetroLabel7.TabIndex = 21
        Me.MetroLabel7.Text = "Settings:"
        Me.MetroLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tglSettings
        '
        Me.tglSettings.AutoSize = True
        Me.tglSettings.Location = New System.Drawing.Point(117, 110)
        Me.tglSettings.Name = "tglSettings"
        Me.tglSettings.Size = New System.Drawing.Size(80, 17)
        Me.tglSettings.TabIndex = 20
        Me.tglSettings.Text = "Off"
        Me.tglSettings.UseSelectable = True
        '
        'formUserFormView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1025, 502)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.MetroLabel2)
        Me.Controls.Add(Me.tglActive)
        Me.Controls.Add(Me.txtLogin)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.MetroLabel5)
        Me.Controls.Add(Me.lblUserID)
        Me.Controls.Add(Me.MetroLabel3)
        Me.Controls.Add(Me.MetroLabel1)
        Me.Name = "formUserFormView"
        Me.Resizable = False
        Me.ShadowType = MetroFramework.Forms.MetroFormShadowType.None
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "formUserFormView"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents lblUserID As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents txtName As MetroFramework.Controls.MetroTextBox
    Friend WithEvents txtLogin As MetroFramework.Controls.MetroTextBox
    Friend WithEvents tglActive As MetroFramework.Controls.MetroToggle
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents MetroLabel7 As MetroFramework.Controls.MetroLabel
    Friend WithEvents tglSettings As MetroFramework.Controls.MetroToggle
    Friend WithEvents cbxIsadmin As MetroFramework.Controls.MetroCheckBox
End Class
