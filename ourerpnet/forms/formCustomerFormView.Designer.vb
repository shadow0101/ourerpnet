﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class formCustomerFormView
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txtJobPosition = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.txtPhone = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel7 = New MetroFramework.Controls.MetroLabel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.txtMobile = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel8 = New MetroFramework.Controls.MetroLabel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.txtFax = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel9 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.txtStreet = New MetroFramework.Controls.MetroTextBox()
        Me.txtCity = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.txtZip = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel6 = New MetroFramework.Controls.MetroLabel()
        Me.txtEmail = New MetroFramework.Controls.MetroTextBox()
        Me.txtName = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel10 = New MetroFramework.Controls.MetroLabel()
        Me.tglActive = New MetroFramework.Controls.MetroToggle()
        Me.MetroLabel12 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel4 = New MetroFramework.Controls.MetroLabel()
        Me.lblID = New MetroFramework.Controls.MetroLabel()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.Panel1)
        Me.FlowLayoutPanel1.Controls.Add(Me.Panel2)
        Me.FlowLayoutPanel1.Controls.Add(Me.Panel3)
        Me.FlowLayoutPanel1.Controls.Add(Me.Panel4)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(610, 179)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(278, 161)
        Me.FlowLayoutPanel1.TabIndex = 36
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.txtJobPosition)
        Me.Panel1.Controls.Add(Me.MetroLabel1)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(269, 32)
        Me.Panel1.TabIndex = 14
        '
        'txtJobPosition
        '
        '
        '
        '
        Me.txtJobPosition.CustomButton.Image = Nothing
        Me.txtJobPosition.CustomButton.Location = New System.Drawing.Point(148, 1)
        Me.txtJobPosition.CustomButton.Name = ""
        Me.txtJobPosition.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtJobPosition.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtJobPosition.CustomButton.TabIndex = 1
        Me.txtJobPosition.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtJobPosition.CustomButton.UseSelectable = True
        Me.txtJobPosition.CustomButton.Visible = False
        Me.txtJobPosition.Lines = New String(-1) {}
        Me.txtJobPosition.Location = New System.Drawing.Point(91, 6)
        Me.txtJobPosition.MaxLength = 32767
        Me.txtJobPosition.Name = "txtJobPosition"
        Me.txtJobPosition.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtJobPosition.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtJobPosition.SelectedText = ""
        Me.txtJobPosition.SelectionLength = 0
        Me.txtJobPosition.SelectionStart = 0
        Me.txtJobPosition.ShortcutsEnabled = True
        Me.txtJobPosition.Size = New System.Drawing.Size(170, 23)
        Me.txtJobPosition.TabIndex = 45
        Me.txtJobPosition.UseSelectable = True
        Me.txtJobPosition.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtJobPosition.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(6, 3)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(79, 19)
        Me.MetroLabel1.TabIndex = 39
        Me.MetroLabel1.Text = "Job Position"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(3, 7)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(64, 13)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "Job Position"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.txtPhone)
        Me.Panel2.Controls.Add(Me.MetroLabel7)
        Me.Panel2.Controls.Add(Me.Label8)
        Me.Panel2.Location = New System.Drawing.Point(3, 41)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(269, 33)
        Me.Panel2.TabIndex = 16
        '
        'txtPhone
        '
        '
        '
        '
        Me.txtPhone.CustomButton.Image = Nothing
        Me.txtPhone.CustomButton.Location = New System.Drawing.Point(183, 1)
        Me.txtPhone.CustomButton.Name = ""
        Me.txtPhone.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtPhone.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtPhone.CustomButton.TabIndex = 1
        Me.txtPhone.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtPhone.CustomButton.UseSelectable = True
        Me.txtPhone.CustomButton.Visible = False
        Me.txtPhone.Lines = New String(-1) {}
        Me.txtPhone.Location = New System.Drawing.Point(58, 7)
        Me.txtPhone.MaxLength = 32767
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPhone.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtPhone.SelectedText = ""
        Me.txtPhone.SelectionLength = 0
        Me.txtPhone.SelectionStart = 0
        Me.txtPhone.ShortcutsEnabled = True
        Me.txtPhone.Size = New System.Drawing.Size(205, 23)
        Me.txtPhone.TabIndex = 53
        Me.txtPhone.UseSelectable = True
        Me.txtPhone.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtPhone.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel7
        '
        Me.MetroLabel7.AutoSize = True
        Me.MetroLabel7.Location = New System.Drawing.Point(6, 7)
        Me.MetroLabel7.Name = "MetroLabel7"
        Me.MetroLabel7.Size = New System.Drawing.Size(46, 19)
        Me.MetroLabel7.TabIndex = 52
        Me.MetroLabel7.Text = "Phone"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(3, 7)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(0, 13)
        Me.Label8.TabIndex = 15
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.txtMobile)
        Me.Panel3.Controls.Add(Me.MetroLabel8)
        Me.Panel3.Location = New System.Drawing.Point(3, 80)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(269, 33)
        Me.Panel3.TabIndex = 17
        '
        'txtMobile
        '
        '
        '
        '
        Me.txtMobile.CustomButton.Image = Nothing
        Me.txtMobile.CustomButton.Location = New System.Drawing.Point(183, 1)
        Me.txtMobile.CustomButton.Name = ""
        Me.txtMobile.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtMobile.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtMobile.CustomButton.TabIndex = 1
        Me.txtMobile.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtMobile.CustomButton.UseSelectable = True
        Me.txtMobile.CustomButton.Visible = False
        Me.txtMobile.Lines = New String(-1) {}
        Me.txtMobile.Location = New System.Drawing.Point(58, 4)
        Me.txtMobile.MaxLength = 32767
        Me.txtMobile.Name = "txtMobile"
        Me.txtMobile.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtMobile.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtMobile.SelectedText = ""
        Me.txtMobile.SelectionLength = 0
        Me.txtMobile.SelectionStart = 0
        Me.txtMobile.ShortcutsEnabled = True
        Me.txtMobile.Size = New System.Drawing.Size(205, 23)
        Me.txtMobile.TabIndex = 54
        Me.txtMobile.UseSelectable = True
        Me.txtMobile.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtMobile.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel8
        '
        Me.MetroLabel8.AutoSize = True
        Me.MetroLabel8.Location = New System.Drawing.Point(6, 4)
        Me.MetroLabel8.Name = "MetroLabel8"
        Me.MetroLabel8.Size = New System.Drawing.Size(53, 19)
        Me.MetroLabel8.TabIndex = 53
        Me.MetroLabel8.Text = "Mobile:"
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.txtFax)
        Me.Panel4.Controls.Add(Me.MetroLabel9)
        Me.Panel4.Location = New System.Drawing.Point(3, 119)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(269, 33)
        Me.Panel4.TabIndex = 18
        '
        'txtFax
        '
        '
        '
        '
        Me.txtFax.CustomButton.Image = Nothing
        Me.txtFax.CustomButton.Location = New System.Drawing.Point(183, 1)
        Me.txtFax.CustomButton.Name = ""
        Me.txtFax.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtFax.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtFax.CustomButton.TabIndex = 1
        Me.txtFax.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtFax.CustomButton.UseSelectable = True
        Me.txtFax.CustomButton.Visible = False
        Me.txtFax.Lines = New String(-1) {}
        Me.txtFax.Location = New System.Drawing.Point(56, 3)
        Me.txtFax.MaxLength = 32767
        Me.txtFax.Name = "txtFax"
        Me.txtFax.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFax.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtFax.SelectedText = ""
        Me.txtFax.SelectionLength = 0
        Me.txtFax.SelectionStart = 0
        Me.txtFax.ShortcutsEnabled = True
        Me.txtFax.Size = New System.Drawing.Size(205, 23)
        Me.txtFax.TabIndex = 55
        Me.txtFax.UseSelectable = True
        Me.txtFax.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtFax.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel9
        '
        Me.MetroLabel9.AutoSize = True
        Me.MetroLabel9.Location = New System.Drawing.Point(6, 4)
        Me.MetroLabel9.Name = "MetroLabel9"
        Me.MetroLabel9.Size = New System.Drawing.Size(31, 19)
        Me.MetroLabel9.TabIndex = 54
        Me.MetroLabel9.Text = "Fax:"
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.Location = New System.Drawing.Point(125, 185)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(46, 19)
        Me.MetroLabel2.TabIndex = 39
        Me.MetroLabel2.Text = "Street:"
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.Location = New System.Drawing.Point(125, 221)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(34, 19)
        Me.MetroLabel3.TabIndex = 42
        Me.MetroLabel3.Text = "City:"
        '
        'txtStreet
        '
        '
        '
        '
        Me.txtStreet.CustomButton.Image = Nothing
        Me.txtStreet.CustomButton.Location = New System.Drawing.Point(183, 1)
        Me.txtStreet.CustomButton.Name = ""
        Me.txtStreet.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtStreet.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtStreet.CustomButton.TabIndex = 1
        Me.txtStreet.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtStreet.CustomButton.UseSelectable = True
        Me.txtStreet.CustomButton.Visible = False
        Me.txtStreet.Lines = New String(-1) {}
        Me.txtStreet.Location = New System.Drawing.Point(214, 185)
        Me.txtStreet.MaxLength = 32767
        Me.txtStreet.Name = "txtStreet"
        Me.txtStreet.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtStreet.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtStreet.SelectedText = ""
        Me.txtStreet.SelectionLength = 0
        Me.txtStreet.SelectionStart = 0
        Me.txtStreet.ShortcutsEnabled = True
        Me.txtStreet.Size = New System.Drawing.Size(205, 23)
        Me.txtStreet.TabIndex = 44
        Me.txtStreet.UseSelectable = True
        Me.txtStreet.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtStreet.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'txtCity
        '
        '
        '
        '
        Me.txtCity.CustomButton.Image = Nothing
        Me.txtCity.CustomButton.Location = New System.Drawing.Point(63, 1)
        Me.txtCity.CustomButton.Name = ""
        Me.txtCity.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtCity.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtCity.CustomButton.TabIndex = 1
        Me.txtCity.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtCity.CustomButton.UseSelectable = True
        Me.txtCity.CustomButton.Visible = False
        Me.txtCity.Lines = New String(-1) {}
        Me.txtCity.Location = New System.Drawing.Point(214, 221)
        Me.txtCity.MaxLength = 32767
        Me.txtCity.Name = "txtCity"
        Me.txtCity.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCity.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtCity.SelectedText = ""
        Me.txtCity.SelectionLength = 0
        Me.txtCity.SelectionStart = 0
        Me.txtCity.ShortcutsEnabled = True
        Me.txtCity.Size = New System.Drawing.Size(85, 23)
        Me.txtCity.TabIndex = 45
        Me.txtCity.UseSelectable = True
        Me.txtCity.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtCity.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.Location = New System.Drawing.Point(305, 221)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(31, 19)
        Me.MetroLabel5.TabIndex = 47
        Me.MetroLabel5.Text = "ZIP:"
        '
        'txtZip
        '
        '
        '
        '
        Me.txtZip.CustomButton.Image = Nothing
        Me.txtZip.CustomButton.Location = New System.Drawing.Point(41, 1)
        Me.txtZip.CustomButton.Name = ""
        Me.txtZip.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtZip.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtZip.CustomButton.TabIndex = 1
        Me.txtZip.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtZip.CustomButton.UseSelectable = True
        Me.txtZip.CustomButton.Visible = False
        Me.txtZip.Lines = New String(-1) {}
        Me.txtZip.Location = New System.Drawing.Point(356, 220)
        Me.txtZip.MaxLength = 32767
        Me.txtZip.Name = "txtZip"
        Me.txtZip.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtZip.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtZip.SelectedText = ""
        Me.txtZip.SelectionLength = 0
        Me.txtZip.SelectionStart = 0
        Me.txtZip.ShortcutsEnabled = True
        Me.txtZip.Size = New System.Drawing.Size(63, 23)
        Me.txtZip.TabIndex = 48
        Me.txtZip.UseSelectable = True
        Me.txtZip.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtZip.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel6
        '
        Me.MetroLabel6.AutoSize = True
        Me.MetroLabel6.Location = New System.Drawing.Point(125, 259)
        Me.MetroLabel6.Name = "MetroLabel6"
        Me.MetroLabel6.Size = New System.Drawing.Size(44, 19)
        Me.MetroLabel6.TabIndex = 49
        Me.MetroLabel6.Text = "Email:"
        '
        'txtEmail
        '
        '
        '
        '
        Me.txtEmail.CustomButton.Image = Nothing
        Me.txtEmail.CustomButton.Location = New System.Drawing.Point(183, 1)
        Me.txtEmail.CustomButton.Name = ""
        Me.txtEmail.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtEmail.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtEmail.CustomButton.TabIndex = 1
        Me.txtEmail.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtEmail.CustomButton.UseSelectable = True
        Me.txtEmail.CustomButton.Visible = False
        Me.txtEmail.Lines = New String(-1) {}
        Me.txtEmail.Location = New System.Drawing.Point(214, 259)
        Me.txtEmail.MaxLength = 32767
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtEmail.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtEmail.SelectedText = ""
        Me.txtEmail.SelectionLength = 0
        Me.txtEmail.SelectionStart = 0
        Me.txtEmail.ShortcutsEnabled = True
        Me.txtEmail.Size = New System.Drawing.Size(205, 23)
        Me.txtEmail.TabIndex = 50
        Me.txtEmail.UseSelectable = True
        Me.txtEmail.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtEmail.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'txtName
        '
        '
        '
        '
        Me.txtName.CustomButton.Image = Nothing
        Me.txtName.CustomButton.Location = New System.Drawing.Point(283, 2)
        Me.txtName.CustomButton.Name = ""
        Me.txtName.CustomButton.Size = New System.Drawing.Size(47, 47)
        Me.txtName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtName.CustomButton.TabIndex = 1
        Me.txtName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtName.CustomButton.UseSelectable = True
        Me.txtName.CustomButton.Visible = False
        Me.txtName.Lines = New String(-1) {}
        Me.txtName.Location = New System.Drawing.Point(233, 78)
        Me.txtName.MaxLength = 32767
        Me.txtName.Multiline = True
        Me.txtName.Name = "txtName"
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtName.SelectedText = ""
        Me.txtName.SelectionLength = 0
        Me.txtName.SelectionStart = 0
        Me.txtName.ShortcutsEnabled = True
        Me.txtName.Size = New System.Drawing.Size(333, 52)
        Me.txtName.TabIndex = 51
        Me.txtName.UseSelectable = True
        Me.txtName.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtName.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel10
        '
        Me.MetroLabel10.AutoSize = True
        Me.MetroLabel10.Location = New System.Drawing.Point(687, 30)
        Me.MetroLabel10.Name = "MetroLabel10"
        Me.MetroLabel10.Size = New System.Drawing.Size(51, 19)
        Me.MetroLabel10.TabIndex = 53
        Me.MetroLabel10.Text = "Active: "
        '
        'tglActive
        '
        Me.tglActive.AutoSize = True
        Me.tglActive.Checked = True
        Me.tglActive.CheckState = System.Windows.Forms.CheckState.Checked
        Me.tglActive.Location = New System.Drawing.Point(777, 32)
        Me.tglActive.Name = "tglActive"
        Me.tglActive.Size = New System.Drawing.Size(80, 17)
        Me.tglActive.TabIndex = 52
        Me.tglActive.Text = "On"
        Me.tglActive.UseSelectable = True
        '
        'MetroLabel12
        '
        Me.MetroLabel12.AutoSize = True
        Me.MetroLabel12.Location = New System.Drawing.Point(123, 87)
        Me.MetroLabel12.Name = "MetroLabel12"
        Me.MetroLabel12.Size = New System.Drawing.Size(48, 19)
        Me.MetroLabel12.TabIndex = 55
        Me.MetroLabel12.Text = "Name:"
        '
        'MetroLabel4
        '
        Me.MetroLabel4.AutoSize = True
        Me.MetroLabel4.Location = New System.Drawing.Point(704, 87)
        Me.MetroLabel4.Name = "MetroLabel4"
        Me.MetroLabel4.Size = New System.Drawing.Size(85, 19)
        Me.MetroLabel4.TabIndex = 56
        Me.MetroLabel4.Text = "Customer ID:"
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(803, 87)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(0, 0)
        Me.lblID.TabIndex = 57
        '
        'formCustomerFormView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1025, 502)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblID)
        Me.Controls.Add(Me.MetroLabel4)
        Me.Controls.Add(Me.MetroLabel12)
        Me.Controls.Add(Me.MetroLabel10)
        Me.Controls.Add(Me.tglActive)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.txtEmail)
        Me.Controls.Add(Me.MetroLabel6)
        Me.Controls.Add(Me.txtZip)
        Me.Controls.Add(Me.MetroLabel5)
        Me.Controls.Add(Me.txtCity)
        Me.Controls.Add(Me.txtStreet)
        Me.Controls.Add(Me.MetroLabel3)
        Me.Controls.Add(Me.MetroLabel2)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Movable = False
        Me.Name = "formCustomerFormView"
        Me.Resizable = False
        Me.ShadowType = MetroFramework.Forms.MetroFormShadowType.None
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Style = MetroFramework.MetroColorStyle.Silver
        Me.Text = "Image"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents txtJobPosition As MetroFramework.Controls.MetroTextBox
    Friend WithEvents txtPhone As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel7 As MetroFramework.Controls.MetroLabel
    Friend WithEvents txtMobile As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel8 As MetroFramework.Controls.MetroLabel
    Friend WithEvents txtFax As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel9 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents txtStreet As MetroFramework.Controls.MetroTextBox
    Friend WithEvents txtCity As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents txtZip As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel6 As MetroFramework.Controls.MetroLabel
    Friend WithEvents txtEmail As MetroFramework.Controls.MetroTextBox
    Friend WithEvents txtName As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel10 As MetroFramework.Controls.MetroLabel
    Friend WithEvents tglActive As MetroFramework.Controls.MetroToggle
    Friend WithEvents MetroLabel12 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel4 As MetroFramework.Controls.MetroLabel
    Friend WithEvents lblID As MetroFramework.Controls.MetroLabel
End Class
