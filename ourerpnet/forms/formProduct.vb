﻿Public Class formProduct
    Inherits formViewForm
    Private _fProduct As formProductFormView
    Private PModel As ourerpnetModels.ProductModel
    Private PBLL As ourerpnetBLL.ProductBLL
    Protected Overrides Sub btnDiscard_Click(sender As Object, e As EventArgs)
        MyBase.btnDiscard_Click(sender, e)
        _fProduct.Close()
    End Sub
    Protected Overrides Sub btnCreate_Click(sender As Object, e As EventArgs)
        If Not Application.OpenForms().OfType(Of formProductFormView).Any Then
            MyBase.btnCreate_Click(sender, e)
            Status = "Create"
            _fProduct = New formProductFormView
            With _fProduct
                .TopLevel = False
                .Parent = formCon.Panel2
            End With
            formCon.Panel2.Controls.Add(_fProduct)
            _fProduct.Show()
        End If
    End Sub
    Protected Overrides Sub btnSave_Click(sender As Object, e As EventArgs)
        PBLL = New ourerpnetBLL.ProductBLL
        If Status = "Create" Then
            PModel = New ourerpnetModels.ProductModel
            With PModel
                .Quantity = _fProduct.txtQuantity.Text
                .Name = _fProduct.txtName.Text
                .Active = _fProduct.tglActive.Checked
                .Description = _fProduct.txtDescription.Text
                .Type = _fProduct.cmbType.SelectedValue
                .Barcode = _fProduct.txtBarcode.Text
                .CategoryID = _fProduct.cmbCategory.SelectedValue
                .SalePrice = _fProduct.txtSalePrice.Text
                .Cost = _fProduct.txtCost.Text
                .LocationID = _fProduct.cmbLocation.SelectedValue
                .Weight = _fProduct.txtWeight.Text
                .Volume = _fProduct.txtVolume.Text
                .TaxID = _fProduct.cmbTax.SelectedValue
                .CreateUID = formIndex._currentID
                .WriteUID = formIndex._currentID
            End With
            If PBLL.Create(PModel) = True Then
                _fProduct.Close()
            End If
        ElseIf Status = "Write" Then
            PModel = New ourerpnetModels.ProductModel
            With PModel
                .Quantity = _fProduct.txtQuantity.Text
                .TaxID = _fProduct.cmbTax.SelectedValue
                .Name = _fProduct.txtName.Text
                .Active = _fProduct.tglActive.Checked
                .Description = _fProduct.txtDescription.Text
                .Type = _fProduct.cmbType.ValueMember
                .Barcode = _fProduct.txtBarcode.Text
                .CategoryID = _fProduct.cmbCategory.SelectedValue
                .SalePrice = _fProduct.txtSalePrice.Text
                .Cost = _fProduct.txtCost.Text
                .LocationID = _fProduct.cmbLocation.SelectedValue
                .Weight = _fProduct.txtWeight.Text
                .Volume = _fProduct.txtVolume.Text
                .WriteUID = formIndex._currentID
                .ID = _fProduct.lblID.Text
            End With
            If PBLL.Write(PModel) = True Then
                btnEdit.Visible = True
                For Each Control As Control In _fProduct.Controls
                    Control.Enabled = False
                Next
            Else
                MessageBox.Show("awef")
            End If
        End If
        MyBase.btnSave_Click(sender, e)
    End Sub
    Protected Overrides Sub formViewForm_Load(sender As Object, e As EventArgs)
        MyBase.formViewForm_Load(sender, e)
    End Sub
    Protected Overrides Sub dgvTreeView_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs)
        MyBase.dgvTreeView_CellDoubleClick(sender, e)
        _fProduct = New formProductFormView
        With _fProduct
            .TopLevel = False
            .Parent = formCon.Panel2
            .txtName.DataBindings.Add("Text", BS, "Name")
            .tglActive.DataBindings.Add("Checked", BS, "Active")
            .cmbType.DataBindings.Add("SelectedValue", BS, "Type")
            .txtBarcode.DataBindings.Add("Text", BS, "Barcode")
            .txtSalePrice.DataBindings.Add("Text", BS, "SalePrice")
            .txtCost.DataBindings.Add("Text", BS, "Cost")
            .cmbCategory.DataBindings.Add("SelectedValue", BS, "CategoryID")
            .cmbLocation.DataBindings.Add("SelectedValue", BS, "LocationID")
            .cmbTax.DataBindings.Add("SelectedValue", BS, "TaxID")
            .txtWeight.DataBindings.Add("Text", BS, "Weight")
            .txtVolume.DataBindings.Add("Text", BS, "Volume")
            .txtDescription.DataBindings.Add("Text", BS, "Description")
            .lblID.DataBindings.Add("Text", BS, "ID")
            .txtQuantity.DataBindings.Add("Text", BS, "Quantity")
        End With
        For Each Control As Control In _fProduct.Controls
            Control.Enabled = False
        Next
        formCon.Panel2.Controls.Add(_fProduct)
        _fProduct.Show()
    End Sub
    Protected Overrides Sub btnEdit_Click(sender As Object, e As EventArgs)
        MyBase.btnEdit_Click(sender, e)
        For Each Control As Control In _fProduct.Controls
            Control.Enabled = True
        Next
    End Sub
    Protected Overrides Sub DeleteToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Form = "product"
        MyBase.DeleteToolStripMenuItem_Click(sender, e)
    End Sub
    Protected Overrides Sub Reload()
        PBLL = New ourerpnetBLL.ProductBLL
        BS.DataSource = PBLL.Products()
        MyBase.Reload()
    End Sub
End Class
