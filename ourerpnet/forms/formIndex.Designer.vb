﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class formIndex
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MetroTabControl1 = New MetroFramework.Controls.MetroTabControl()
        Me.MetroTabPage1 = New MetroFramework.Controls.MetroTabPage()
        Me.SplitContainer3 = New System.Windows.Forms.SplitContainer()
        Me.MetroTile10 = New MetroFramework.Controls.MetroTile()
        Me.MetroTile9 = New MetroFramework.Controls.MetroTile()
        Me.MetroTile8 = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.tileProducts = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.MetroTabPage4 = New MetroFramework.Controls.MetroTabPage()
        Me.SplitContainer4 = New System.Windows.Forms.SplitContainer()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.MetroTile7 = New MetroFramework.Controls.MetroTile()
        Me.MetroTile5 = New MetroFramework.Controls.MetroTile()
        Me.MetroTile6 = New MetroFramework.Controls.MetroTile()
        Me.MetroTabPage3 = New MetroFramework.Controls.MetroTabPage()
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer()
        Me.MetroTile11 = New MetroFramework.Controls.MetroTile()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.MetroTile3 = New MetroFramework.Controls.MetroTile()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.MetroTabPage2 = New MetroFramework.Controls.MetroTabPage()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.tileusers = New MetroFramework.Controls.MetroTile()
        Me.MetroTile2 = New MetroFramework.Controls.MetroTile()
        Me.MetroTile4 = New MetroFramework.Controls.MetroTile()
        Me.MetroTabControl1.SuspendLayout()
        Me.MetroTabPage1.SuspendLayout()
        CType(Me.SplitContainer3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer3.Panel1.SuspendLayout()
        Me.SplitContainer3.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MetroTabPage4.SuspendLayout()
        CType(Me.SplitContainer4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer4.Panel1.SuspendLayout()
        Me.SplitContainer4.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MetroTabPage3.SuspendLayout()
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MetroTabPage2.SuspendLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MetroTabControl1
        '
        Me.MetroTabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons
        Me.MetroTabControl1.Controls.Add(Me.MetroTabPage1)
        Me.MetroTabControl1.Controls.Add(Me.MetroTabPage4)
        Me.MetroTabControl1.Controls.Add(Me.MetroTabPage3)
        Me.MetroTabControl1.Controls.Add(Me.MetroTabPage2)
        Me.MetroTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MetroTabControl1.Location = New System.Drawing.Point(20, 60)
        Me.MetroTabControl1.Name = "MetroTabControl1"
        Me.MetroTabControl1.SelectedIndex = 3
        Me.MetroTabControl1.Size = New System.Drawing.Size(1240, 640)
        Me.MetroTabControl1.TabIndex = 3
        Me.MetroTabControl1.UseCustomBackColor = True
        Me.MetroTabControl1.UseSelectable = True
        '
        'MetroTabPage1
        '
        Me.MetroTabPage1.Controls.Add(Me.SplitContainer3)
        Me.MetroTabPage1.HorizontalScrollbarBarColor = True
        Me.MetroTabPage1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroTabPage1.HorizontalScrollbarSize = 10
        Me.MetroTabPage1.Location = New System.Drawing.Point(4, 41)
        Me.MetroTabPage1.Name = "MetroTabPage1"
        Me.MetroTabPage1.Size = New System.Drawing.Size(1232, 595)
        Me.MetroTabPage1.TabIndex = 0
        Me.MetroTabPage1.Text = "Inventory"
        Me.MetroTabPage1.VerticalScrollbarBarColor = True
        Me.MetroTabPage1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroTabPage1.VerticalScrollbarSize = 10
        '
        'SplitContainer3
        '
        Me.SplitContainer3.IsSplitterFixed = True
        Me.SplitContainer3.Location = New System.Drawing.Point(3, 3)
        Me.SplitContainer3.Name = "SplitContainer3"
        '
        'SplitContainer3.Panel1
        '
        Me.SplitContainer3.Panel1.Controls.Add(Me.MetroTile10)
        Me.SplitContainer3.Panel1.Controls.Add(Me.MetroTile9)
        Me.SplitContainer3.Panel1.Controls.Add(Me.MetroTile8)
        Me.SplitContainer3.Panel1.Controls.Add(Me.MetroLabel2)
        Me.SplitContainer3.Panel1.Controls.Add(Me.PictureBox2)
        Me.SplitContainer3.Panel1.Controls.Add(Me.tileProducts)
        Me.SplitContainer3.Panel1.Controls.Add(Me.MetroLabel1)
        Me.SplitContainer3.Size = New System.Drawing.Size(1226, 589)
        Me.SplitContainer3.SplitterDistance = 197
        Me.SplitContainer3.TabIndex = 4
        '
        'MetroTile10
        '
        Me.MetroTile10.ActiveControl = Nothing
        Me.MetroTile10.Location = New System.Drawing.Point(3, 305)
        Me.MetroTile10.Name = "MetroTile10"
        Me.MetroTile10.Size = New System.Drawing.Size(191, 43)
        Me.MetroTile10.TabIndex = 8
        Me.MetroTile10.Text = "Taxes"
        Me.MetroTile10.UseSelectable = True
        '
        'MetroTile9
        '
        Me.MetroTile9.ActiveControl = Nothing
        Me.MetroTile9.Location = New System.Drawing.Point(3, 190)
        Me.MetroTile9.Name = "MetroTile9"
        Me.MetroTile9.Size = New System.Drawing.Size(191, 43)
        Me.MetroTile9.TabIndex = 7
        Me.MetroTile9.Text = "Locations"
        Me.MetroTile9.UseSelectable = True
        '
        'MetroTile8
        '
        Me.MetroTile8.ActiveControl = Nothing
        Me.MetroTile8.Location = New System.Drawing.Point(3, 261)
        Me.MetroTile8.Name = "MetroTile8"
        Me.MetroTile8.Size = New System.Drawing.Size(191, 43)
        Me.MetroTile8.TabIndex = 6
        Me.MetroTile8.Text = "Product Categories"
        Me.MetroTile8.UseSelectable = True
        '
        'MetroLabel2
        '
        Me.MetroLabel2.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.MetroLabel2.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.MetroLabel2.Location = New System.Drawing.Point(3, 234)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(191, 26)
        Me.MetroLabel2.TabIndex = 5
        Me.MetroLabel2.Text = "Configuration"
        Me.MetroLabel2.UseCustomBackColor = True
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.ourerpnet.My.Resources.Resources.kisspng_inventory_fifo_and_lifo_accounting_logistics_asset_scanner_5ab73fb88b2761_78516537152195884057
        Me.PictureBox2.Location = New System.Drawing.Point(3, 3)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(191, 111)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 4
        Me.PictureBox2.TabStop = False
        '
        'tileProducts
        '
        Me.tileProducts.ActiveControl = Nothing
        Me.tileProducts.Location = New System.Drawing.Point(3, 146)
        Me.tileProducts.Name = "tileProducts"
        Me.tileProducts.Size = New System.Drawing.Size(191, 43)
        Me.tileProducts.TabIndex = 1
        Me.tileProducts.Text = "Products"
        Me.tileProducts.UseSelectable = True
        '
        'MetroLabel1
        '
        Me.MetroLabel1.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.MetroLabel1.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.MetroLabel1.Location = New System.Drawing.Point(3, 119)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(191, 26)
        Me.MetroLabel1.TabIndex = 0
        Me.MetroLabel1.Text = "Inventory Control"
        Me.MetroLabel1.UseCustomBackColor = True
        '
        'MetroTabPage4
        '
        Me.MetroTabPage4.Controls.Add(Me.SplitContainer4)
        Me.MetroTabPage4.HorizontalScrollbarBarColor = True
        Me.MetroTabPage4.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroTabPage4.HorizontalScrollbarSize = 10
        Me.MetroTabPage4.Location = New System.Drawing.Point(4, 41)
        Me.MetroTabPage4.Name = "MetroTabPage4"
        Me.MetroTabPage4.Size = New System.Drawing.Size(1232, 595)
        Me.MetroTabPage4.TabIndex = 3
        Me.MetroTabPage4.Text = "Sales"
        Me.MetroTabPage4.VerticalScrollbarBarColor = True
        Me.MetroTabPage4.VerticalScrollbarHighlightOnWheel = False
        Me.MetroTabPage4.VerticalScrollbarSize = 10
        '
        'SplitContainer4
        '
        Me.SplitContainer4.IsSplitterFixed = True
        Me.SplitContainer4.Location = New System.Drawing.Point(3, 3)
        Me.SplitContainer4.Name = "SplitContainer4"
        '
        'SplitContainer4.Panel1
        '
        Me.SplitContainer4.Panel1.BackColor = System.Drawing.Color.Snow
        Me.SplitContainer4.Panel1.Controls.Add(Me.PictureBox3)
        Me.SplitContainer4.Panel1.Controls.Add(Me.MetroTile7)
        Me.SplitContainer4.Panel1.Controls.Add(Me.MetroTile5)
        Me.SplitContainer4.Panel1.Controls.Add(Me.MetroTile6)
        '
        'SplitContainer4.Panel2
        '
        Me.SplitContainer4.Panel2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.SplitContainer4.Size = New System.Drawing.Size(1226, 589)
        Me.SplitContainer4.SplitterDistance = 197
        Me.SplitContainer4.TabIndex = 4
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = Global.ourerpnet.My.Resources.Resources.download
        Me.PictureBox3.Location = New System.Drawing.Point(3, 3)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(191, 111)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox3.TabIndex = 3
        Me.PictureBox3.TabStop = False
        '
        'MetroTile7
        '
        Me.MetroTile7.ActiveControl = Nothing
        Me.MetroTile7.Location = New System.Drawing.Point(3, 208)
        Me.MetroTile7.Name = "MetroTile7"
        Me.MetroTile7.Size = New System.Drawing.Size(191, 43)
        Me.MetroTile7.TabIndex = 2
        Me.MetroTile7.Text = "Sales Orders"
        Me.MetroTile7.UseSelectable = True
        '
        'MetroTile5
        '
        Me.MetroTile5.ActiveControl = Nothing
        Me.MetroTile5.Location = New System.Drawing.Point(3, 164)
        Me.MetroTile5.Name = "MetroTile5"
        Me.MetroTile5.Size = New System.Drawing.Size(191, 43)
        Me.MetroTile5.TabIndex = 1
        Me.MetroTile5.Text = "Quotations"
        Me.MetroTile5.UseSelectable = True
        '
        'MetroTile6
        '
        Me.MetroTile6.ActiveControl = Nothing
        Me.MetroTile6.Location = New System.Drawing.Point(3, 120)
        Me.MetroTile6.Name = "MetroTile6"
        Me.MetroTile6.Size = New System.Drawing.Size(191, 43)
        Me.MetroTile6.TabIndex = 0
        Me.MetroTile6.Text = "Customers"
        Me.MetroTile6.UseSelectable = True
        '
        'MetroTabPage3
        '
        Me.MetroTabPage3.Controls.Add(Me.SplitContainer2)
        Me.MetroTabPage3.HorizontalScrollbarBarColor = True
        Me.MetroTabPage3.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroTabPage3.HorizontalScrollbarSize = 10
        Me.MetroTabPage3.Location = New System.Drawing.Point(4, 41)
        Me.MetroTabPage3.Name = "MetroTabPage3"
        Me.MetroTabPage3.Size = New System.Drawing.Size(1232, 595)
        Me.MetroTabPage3.TabIndex = 2
        Me.MetroTabPage3.Text = "Application"
        Me.MetroTabPage3.VerticalScrollbarBarColor = True
        Me.MetroTabPage3.VerticalScrollbarHighlightOnWheel = False
        Me.MetroTabPage3.VerticalScrollbarSize = 10
        '
        'SplitContainer2
        '
        Me.SplitContainer2.IsSplitterFixed = True
        Me.SplitContainer2.Location = New System.Drawing.Point(3, 3)
        Me.SplitContainer2.Name = "SplitContainer2"
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.Controls.Add(Me.MetroTile11)
        Me.SplitContainer2.Panel1.Controls.Add(Me.PictureBox1)
        Me.SplitContainer2.Panel1.Controls.Add(Me.MetroTile3)
        Me.SplitContainer2.Panel1.Controls.Add(Me.MetroTile1)
        Me.SplitContainer2.Size = New System.Drawing.Size(1226, 589)
        Me.SplitContainer2.SplitterDistance = 197
        Me.SplitContainer2.TabIndex = 3
        '
        'MetroTile11
        '
        Me.MetroTile11.ActiveControl = Nothing
        Me.MetroTile11.Location = New System.Drawing.Point(3, 164)
        Me.MetroTile11.Name = "MetroTile11"
        Me.MetroTile11.Size = New System.Drawing.Size(191, 43)
        Me.MetroTile11.TabIndex = 4
        Me.MetroTile11.Text = "History"
        Me.MetroTile11.UseSelectable = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(3, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(191, 111)
        Me.PictureBox1.TabIndex = 3
        Me.PictureBox1.TabStop = False
        '
        'MetroTile3
        '
        Me.MetroTile3.ActiveControl = Nothing
        Me.MetroTile3.Location = New System.Drawing.Point(3, 208)
        Me.MetroTile3.Name = "MetroTile3"
        Me.MetroTile3.Size = New System.Drawing.Size(191, 43)
        Me.MetroTile3.TabIndex = 1
        Me.MetroTile3.Text = "Log Out"
        Me.MetroTile3.UseSelectable = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.Location = New System.Drawing.Point(3, 120)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(191, 43)
        Me.MetroTile1.TabIndex = 0
        Me.MetroTile1.Text = "Profile"
        Me.MetroTile1.UseSelectable = True
        '
        'MetroTabPage2
        '
        Me.MetroTabPage2.Controls.Add(Me.SplitContainer1)
        Me.MetroTabPage2.HorizontalScrollbarBarColor = True
        Me.MetroTabPage2.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroTabPage2.HorizontalScrollbarSize = 10
        Me.MetroTabPage2.Location = New System.Drawing.Point(4, 41)
        Me.MetroTabPage2.Name = "MetroTabPage2"
        Me.MetroTabPage2.Size = New System.Drawing.Size(1232, 595)
        Me.MetroTabPage2.TabIndex = 1
        Me.MetroTabPage2.Text = "Settings"
        Me.MetroTabPage2.VerticalScrollbarBarColor = True
        Me.MetroTabPage2.VerticalScrollbarHighlightOnWheel = False
        Me.MetroTabPage2.VerticalScrollbarSize = 10
        '
        'SplitContainer1
        '
        Me.SplitContainer1.IsSplitterFixed = True
        Me.SplitContainer1.Location = New System.Drawing.Point(3, 3)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.PictureBox4)
        Me.SplitContainer1.Panel1.Controls.Add(Me.MetroTile4)
        Me.SplitContainer1.Panel1.Controls.Add(Me.tileusers)
        Me.SplitContainer1.Panel1.Controls.Add(Me.MetroTile2)
        Me.SplitContainer1.Size = New System.Drawing.Size(1226, 589)
        Me.SplitContainer1.SplitterDistance = 197
        Me.SplitContainer1.TabIndex = 2
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = Global.ourerpnet.My.Resources.Resources.gear_wrench_flat_icon_filled_vector_sign_colorful_pictogram_isolated_white_settings_symbol_logo_illustration_style_95289408
        Me.PictureBox4.Location = New System.Drawing.Point(3, 3)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(191, 111)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox4.TabIndex = 6
        Me.PictureBox4.TabStop = False
        '
        'tileusers
        '
        Me.tileusers.ActiveControl = Nothing
        Me.tileusers.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tileusers.Location = New System.Drawing.Point(3, 120)
        Me.tileusers.Name = "tileusers"
        Me.tileusers.Size = New System.Drawing.Size(191, 46)
        Me.tileusers.TabIndex = 3
        Me.tileusers.Text = "Users"
        Me.tileusers.UseSelectable = True
        '
        'MetroTile2
        '
        Me.MetroTile2.ActiveControl = Nothing
        Me.MetroTile2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.MetroTile2.Location = New System.Drawing.Point(3, 167)
        Me.MetroTile2.Name = "MetroTile2"
        Me.MetroTile2.Size = New System.Drawing.Size(191, 43)
        Me.MetroTile2.TabIndex = 4
        Me.MetroTile2.Text = "General Settings"
        Me.MetroTile2.UseSelectable = True
        '
        'MetroTile4
        '
        Me.MetroTile4.ActiveControl = Nothing
        Me.MetroTile4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.MetroTile4.Location = New System.Drawing.Point(3, 211)
        Me.MetroTile4.Name = "MetroTile4"
        Me.MetroTile4.Size = New System.Drawing.Size(191, 46)
        Me.MetroTile4.TabIndex = 5
        Me.MetroTile4.Text = "Backup"
        Me.MetroTile4.UseSelectable = True
        '
        'formIndex
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1280, 720)
        Me.Controls.Add(Me.MetroTabControl1)
        Me.IsMdiContainer = True
        Me.Name = "formIndex"
        Me.Resizable = False
        Me.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow
        Me.Text = "FM Trading Management System"
        Me.TransparencyKey = System.Drawing.Color.Empty
        Me.MetroTabControl1.ResumeLayout(False)
        Me.MetroTabPage1.ResumeLayout(False)
        Me.SplitContainer3.Panel1.ResumeLayout(False)
        CType(Me.SplitContainer3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer3.ResumeLayout(False)
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MetroTabPage4.ResumeLayout(False)
        Me.SplitContainer4.Panel1.ResumeLayout(False)
        CType(Me.SplitContainer4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer4.ResumeLayout(False)
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MetroTabPage3.ResumeLayout(False)
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer2.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MetroTabPage2.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents MetroTabControl1 As MetroFramework.Controls.MetroTabControl
    Friend WithEvents MetroTabPage1 As MetroFramework.Controls.MetroTabPage
    Friend WithEvents MetroTabPage3 As MetroFramework.Controls.MetroTabPage
    Friend WithEvents MetroTabPage2 As MetroFramework.Controls.MetroTabPage
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents tileusers As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile2 As MetroFramework.Controls.MetroTile
    Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
    Friend WithEvents MetroTile3 As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents SplitContainer3 As System.Windows.Forms.SplitContainer
    Friend WithEvents tileProducts As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroTabPage4 As MetroFramework.Controls.MetroTabPage
    Friend WithEvents SplitContainer4 As System.Windows.Forms.SplitContainer
    Friend WithEvents MetroTile6 As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile5 As MetroFramework.Controls.MetroTile
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents MetroTile7 As MetroFramework.Controls.MetroTile
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents MetroTile10 As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile9 As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile8 As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroTile11 As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile4 As MetroFramework.Controls.MetroTile
End Class
