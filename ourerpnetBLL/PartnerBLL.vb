﻿Imports ourerpnetDAL
Public Class PartnerBLL
    Private pDAL As PartnerDAL
    Public Overloads Function Delete(ByVal id As Integer) As Boolean
        pDAL = New PartnerDAL
        Return pDAL.delete("partner", id)
    End Function
    Public Function Write(ByVal oPartner As ourerpnetModels.PartnerModel) As Boolean
        pDAL = New PartnerDAL
        Return pDAL.write(oPartner)
    End Function
    Public Function Partners() As List(Of ourerpnetModels.PartnerModel)
        pDAL = New PartnerDAL
        Return pDAL.all()
    End Function
    Public Function Create(ByVal oPartner As ourerpnetModels.PartnerModel) As Boolean
        pDAL = New PartnerDAL
        Return pDAL.create(oPartner)
    End Function
End Class
