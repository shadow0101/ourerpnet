﻿Imports ourerpnetDAL
Imports System.Security.AccessControl
Public Class BaseBLL
    Private BDAL As BaseDAL
    Public Overloads Function Delete(ByVal table As String, ByVal id As Integer) As Boolean
        BDAL = New BaseDAL
        Return BDAL.delete(table, id)
    End Function
    Public Function UpdatePath(ByVal path As String) As Boolean
        BDAL = New BaseDAL
        path = path.Replace("\", "/")
        Return BDAL.write(path)
    End Function
    Public Function Settings() As ourerpnetModels.SettingModel
        BDAL = New BaseDAL
        Return BDAL.getSettings()
    End Function
    Public Function Access(ByVal UserAccount As String) As DirectorySecurity
        Dim result As New DirectorySecurity
        result.AddAccessRule(New FileSystemAccessRule(UserAccount, FileSystemRights.Modify, InheritanceFlags.ContainerInherit Or InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow))
        result.SetAccessRuleProtection(True, False)
        Return result
    End Function
End Class
