﻿Imports ourerpnetDAL
Imports ourerpnetModels
Public Class SaleOrderBLL
    Inherits BaseBLL
    Private Saleorder As SaleOrderDAL
    Private Orders As New SaleOrderModel
    Private item As OrderLineModel
    Public Sub PartnerID(ByVal id As Integer)
        Orders.PartnerID = id
    End Sub
    Public Sub BackupSaleOrders()
        Saleorder = New SaleOrderDAL
        Saleorder.backup()
    End Sub
    Public Function Create(ByVal uID As Integer) As Boolean
        Saleorder = New SaleOrderDAL
        Orders.CreateUID = uID
        Orders.WriteUID = uID
        Orders.UserID = uID
        Orders.Status = "quot"
        Return Saleorder.create(Orders)
    End Function
    Public Function LastQuantity(ByVal id As Integer) As Integer
        Dim result As Integer
        For Each Me.item In Orders.OrderLine
            If id = item.ProductID Then
                result = item.Quantity
                Exit For
            End If
        Next
        Return result
    End Function
    Public Function UntaxedAmount() As Decimal
        Dim result As Decimal
        For Each item As OrderLineModel In Orders.OrderLine
            result += item.PriceSubtotal()
        Next
        Return result
    End Function
    Public Function Total() As Decimal
        Dim result As Decimal
        result = UntaxedAmount() + Taxes()
        Return result
    End Function
    Private Sub AddOrder(ByVal val As OrderLineModel)
        Orders.AddOrderLine = val
    End Sub
    Public Function Update(ByVal pID As Integer, ByVal qty As Integer) As Boolean
        Dim result As Boolean = False
        For Each Me.item In Orders.OrderLine
            If pID = item.ProductID Then
                item.Quantity = qty
                item.PriceSubtotal = item.Quantity * item.PriceUnit
                item.TaxedAmount = item.PriceSubtotal * item.PriceTax
                result = True
                Exit For
            End If
        Next
        Return result
    End Function
    Public Function Add(ByVal val As OrderLineModel) As Boolean
        Dim result As Boolean = False
        If Orders.OrderLine.Count > 0 Then
            For Each item As OrderLineModel In Orders.OrderLine
                If val.ProductID = item.ProductID Then
                    Throw New Exception("Product is already in OrderLine")
                    Return False
                    Exit Function
                End If
            Next
        End If
        AddOrder(val)
        result = True
        Return result
    End Function
    Public Function Taxes() As Decimal
        Dim result As Decimal
        For Each item As OrderLineModel In Orders.OrderLine
            result += item.TaxedAmount()
        Next
        Return result
    End Function
    Public Function SaleOrders() As List(Of SaleOrderModel)
        Saleorder = New SaleOrderDAL
        Return Saleorder.all()
    End Function
End Class
