﻿Imports System.Security.Cryptography
Imports System.Text
Imports ourerpnetDAL
Public Class UserBLL
    Private UsersDAL As UserDAL
    Public Function Update(ByVal updateUser As ourerpnetModels.UserModel) As Boolean
        UsersDAL = New UserDAL
        Return UsersDAL.write(updateUser)
    End Function
    Public Function Create(ByVal newUser As ourerpnetModels.UserModel) As Boolean
        UsersDAL = New UserDAL
        Return UsersDAL.create(newUser)
    End Function
    Public Function Browse(ByVal id As Integer) As ourerpnetModels.UserModel
        UsersDAL = New UserDAL
        Dim User As ourerpnetModels.UserModel = UsersDAL.BrowseID(id)
        If IsNothing(User) Then
            Throw New Exception("Nothing")
        End If
        Return User
    End Function
    Public Function LoginUser(ByVal login As String, ByVal password As String) As ourerpnetModels.UserModel
        UsersDAL = New UserDAL
        Dim User As ourerpnetModels.UserModel
        User = UsersDAL.CheckUser(login, Hash512(password))
        If User.PasswordCrypt = Hash512(password) Then
            Return User
        Else
            Throw New Exception("Wrong login/password")
        End If
    End Function
    Public Function Users() As List(Of ourerpnetModels.UserModel)
        UsersDAL = New UserDAL
        Return UsersDAL.all()
    End Function
    Private Function Hash512(password As String) As String
        Dim convertedToBytes As Byte() = Encoding.UTF8.GetBytes(password)
        Dim hashType As HashAlgorithm = New SHA512Managed()
        Dim hashBytes As Byte() = hashType.ComputeHash(convertedToBytes)
        Dim hashedResult As String = Convert.ToBase64String(hashBytes)
        Return hashedResult
    End Function
    Public Function Logs(ByVal user As ourerpnetModels.UserModel) As List(Of ourerpnetModels.UserLogModel)
        UsersDAL = New UserDAL
        If user.IsAdmin = True Then
            Return UsersDAL.all("users_log")
        Else
            Return UsersDAL.browseLogs(user.ID)
        End If
    End Function
End Class
