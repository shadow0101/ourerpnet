﻿Imports ourerpnetDAL
Public Class ProductBLL
    Private pDAL As ProductDAL
    Private pModel As ourerpnetModels.ProductModel
    Public Function SubTotal(ByVal num1 As Decimal, ByVal num2 As Decimal) As Decimal
        Return num1 * num2
    End Function
    Public Overloads Function Create(ByVal oProduct As ourerpnetModels.ProductModel) As Boolean
        pDAL = New ProductDAL
        Return pDAL.create(oProduct)
    End Function
    Public Function CreateLocation(ByVal oLocation As ourerpnetModels.ProductModel) As Boolean
        pDAL = New ProductDAL
        Return pDAL.create("stock_location", oLocation)
    End Function
    Public Function CreateProduct(ByVal oLocation As ourerpnetModels.ProductModel) As Boolean
        pDAL = New ProductDAL
        Return pDAL.create("product_category", oLocation)
    End Function
    Public Function Write(ByVal oProduct As ourerpnetModels.ProductModel) As Boolean
        pDAL = New ProductDAL
        Return pDAL.write(oProduct)
    End Function
 
    Public Sub CheckQuantity(ByVal id As Integer, ByVal qty As Integer)
        pDAL = New ProductDAL
        pModel = pDAL.BrowseID(id)
        If pModel.Quantity < qty Then
            Throw New Exception("Not enough quantity, Product has qty of " & pModel.Quantity)
        End If
    End Sub
    Public Function Browse(ByVal id As Integer) As ourerpnetModels.ProductModel
        pDAL = New ProductDAL
        Dim prod As ourerpnetModels.ProductModel = pDAL.BrowseID(id)
        If IsNothing(prod) Then
            Throw New Exception("Nothing")
        End If
        If prod.Quantity <= 0 Then
            Throw New Exception("Product not enough stock")
        End If
        Return prod
    End Function
    Public Function Products() As List(Of ourerpnetModels.ProductModel)
        pDAL = New ProductDAL
        Return pDAL.all()
    End Function
    Public Function Locations() As List(Of ourerpnetModels.ProductModel)
        pDAL = New ProductDAL
        Return pDAL.productForeign("stock_location")
    End Function
    Public Function Taxes() As List(Of ourerpnetModels.ProductModel)
        pDAL = New ProductDAL
        Return pDAL.productForeign("account_tax")
    End Function
    Public Function Categories() As List(Of ourerpnetModels.ProductModel)
        pDAL = New ProductDAL
        Return pDAL.productForeign("product_category")
    End Function
    Public Function getCategories() As Dictionary(Of Integer, String)
        pDAL = New ProductDAL
        Return pDAL.getTable("product_category")
    End Function
    Public Function getLocations() As Dictionary(Of Integer, String)
        pDAL = New ProductDAL
        Return pDAL.getTable("stock_location")
    End Function
    Public Function getTaxes() As Dictionary(Of Integer, String)
        pDAL = New ProductDAL
        Return pDAL.getTable("account_tax")
    End Function
End Class
