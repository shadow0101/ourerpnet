﻿Public Class ProductModel
    Inherits BaseModel
    'Inventory
    Private _quantity As Integer


    'Tax
    Private _taxName As String
    Private _taxID As Integer
    Private _taxAmount As Decimal
    'Location
    Private _locationID As Integer
    Private _locationName As String
    'Category
    Private _categoryName As String
    Private _categoryID As Integer
    'Product
    Private _description As String
    Private _type As String
    Private _barcode As String
    Private _cost As Decimal
    Private _saleprice As Decimal
    Private _routes As Boolean
    Private _weight As Decimal
    Private _volume As Decimal
    Private _income As String
    Public Property Quantity As Integer
        Get
            Return _quantity
        End Get
        Set(value As Integer)
            _quantity = value
        End Set
    End Property
    Public Property LocationID As Integer
        Get
            Return _locationID
        End Get
        Set(value As Integer)
            _locationID = value
        End Set
    End Property
    Public Property CategoryID As Integer
        Get
            Return _categoryID
        End Get
        Set(value As Integer)
            _categoryID = value
        End Set
    End Property
    Public Property CategoryName As String
        Get
            Return _categoryName
        End Get
        Set(value As String)
            _categoryName = value
        End Set
    End Property
    Public Property LocationName As String
        Get
            Return _locationName
        End Get
        Set(value As String)
            _locationName = value
        End Set
    End Property
    Public Property TaxAmount As Decimal
        Get
            Return _taxAmount
        End Get
        Set(value As Decimal)
            _taxAmount = value
        End Set
    End Property
    Public Property TaxName As String
        Get
            Return _taxName
        End Get
        Set(value As String)
            _taxName = value
        End Set
    End Property
    Public Property TaxID As Integer
        Get
            Return _taxID
        End Get
        Set(value As Integer)
            _taxID = value
        End Set
    End Property
    Public Property Description As String
        Get
            Return _description
        End Get
        Set(value As String)
            _description = value
        End Set
    End Property
    Public ReadOnly Property TypeList As Dictionary(Of String, String)
        Get
            Dim tl As New Dictionary(Of String, String)
            tl.Add("product", "Stockable Product")
            tl.Add("consume", "Consumable")
            tl.Add("service", "Service")
            Return tl
        End Get
    End Property
    Public Property Type As String
        Get
            Return _type
        End Get
        Set(value As String)
            _type = value
        End Set
    End Property
    Public Property Barcode As String
        Get
            Return _barcode
        End Get
        Set(value As String)
            _barcode = value
        End Set
    End Property
    Public Property Cost As Decimal
        Get
            Return _cost
        End Get
        Set(value As Decimal)
            _cost = value
        End Set
    End Property
    Public Property SalePrice As Decimal
        Get
            Return _saleprice
        End Get
        Set(value As Decimal)
            _saleprice = value
        End Set
    End Property
    Public Property Routes As Boolean
        Get
            Return _routes
        End Get
        Set(value As Boolean)
            _routes = value
        End Set
    End Property
    Public Property Weight As Decimal
        Get
            Return _weight
        End Get
        Set(value As Decimal)
            _weight = CDec(value)
        End Set
    End Property
    Public Property Volume As Decimal
        Get
            Return _volume
        End Get
        Set(value As Decimal)
            _volume = value
        End Set
    End Property
    Public Property Income As Decimal
        Get
            Return _income
        End Get
        Set(value As Decimal)
            _income = value
        End Set
    End Property
End Class
