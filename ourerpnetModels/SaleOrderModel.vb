﻿Public Class OrderLineModel
    Private _productID As Integer
    Private _orderID As Integer
    Private _priceUnit As Decimal
    Private _quantity As Integer
    Private _priceSubtotal As Decimal
    Private _priceTax As Decimal
    Private _taxed As Decimal
    Public Property TaxedAmount As Decimal
        Get
            Return _taxed
        End Get
        Set(value As Decimal)
            _taxed = value
        End Set
    End Property
    Public Property PriceUnit As Decimal
        Get
            Return _priceUnit
        End Get
        Set(value As Decimal)
            _priceUnit = value
        End Set
    End Property
    Public Sub New(ByVal productId As Integer, ByVal priceUnit As Decimal, ByVal quantity As Integer, ByVal tax As Decimal)
        Me.ProductID = productId
        Me.PriceUnit = priceUnit
        Me.Quantity = quantity
        Me.PriceTax = tax
        Me.PriceSubtotal = Me.Quantity * Me.PriceUnit
        Me.TaxedAmount = PriceSubtotal * PriceTax
    End Sub
    Public Property ProductID As Integer
        Get
            Return _productID
        End Get
        Set(value As Integer)
            _productID = value
        End Set
    End Property
    Public Property OrderID As Integer
        Get
            Return _orderID
        End Get
        Set(value As Integer)
            _orderID = value
        End Set
    End Property
    Public Property Quantity As Integer
        Get
            Return _quantity
        End Get
        Set(value As Integer)
            _quantity = value
        End Set
    End Property
    Public Property PriceSubtotal As Decimal
        Get
            Return _priceSubtotal
        End Get
        Set(value As Decimal)
            _priceSubtotal = value
        End Set
    End Property
    Public Property PriceTax As Decimal
        Get
            Return _priceTax
        End Get
        Set(value As Decimal)
            _priceTax = value
        End Set
    End Property
End Class
Public Class SaleOrderModel
    Inherits BaseModel
    Private _orderLine As New List(Of OrderLineModel)
    Private _partner_id As Integer
    Private _date_order As Date
    Private _amount_total As Decimal
    Private _amount_tax As Decimal
    Private _user_id As Integer
    Private _status As String
    Public ReadOnly Property OrderLine As List(Of OrderLineModel)
        Get
            Return _orderLine
        End Get
    End Property
    Public WriteOnly Property AddOrderLine As OrderLineModel
        Set(value As OrderLineModel)
            _orderLine.Add(value)
        End Set
    End Property
    Public Property PartnerID As Integer
        Get
            Return _partner_id
        End Get
        Set(value As Integer)
            _partner_id = value
        End Set
    End Property
    Public Property DateOrder As Date
        Get
            Return _date_order
        End Get
        Set(value As Date)
            _date_order = value
        End Set
    End Property

    Public Property AmountTotal As Decimal
        Get
            Return _amount_total
        End Get
        Set(value As Decimal)
            _amount_total = value
        End Set
    End Property

    Public Property AmountTax As Decimal
        Get
            Return _amount_tax
        End Get
        Set(value As Decimal)
            _amount_tax = value
        End Set
    End Property
    Public Property UserID As Integer
        Get
            Return _user_id
        End Get
        Set(value As Integer)
            _user_id = value
        End Set
    End Property
    Public Property Status As String
        Get
            Return _status
        End Get
        Set(value As String)
            _status = value
        End Set
    End Property
End Class
