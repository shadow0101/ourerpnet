﻿Public Class PartnerModel
    Inherits BaseModel
    Private _street As String
    Private _city As String
    Private _zip As String
    Private _email As String
    Private _jobPosition As String
    Private _phone As String
    Private _mobile As String
    Private _fax As String
    Public Property Street As String
        Get
            Return _street
        End Get
        Set(ByVal value As String)
            _street = value
        End Set
    End Property
    Public Property City As String
        Get
            Return _city
        End Get
        Set(ByVal value As String)
            _city = value
        End Set
    End Property
    Public Property Zip As String
        Get
            Return _zip
        End Get
        Set(ByVal value As String)
            _zip = value
        End Set
    End Property
    Public Property Email As String
        Get
            Return _email
        End Get
        Set(ByVal value As String)
            _email = value
        End Set
    End Property
    Public Property JobPosition As String
        Get
            Return _jobPosition
        End Get
        Set(ByVal value As String)
            _jobPosition = value
        End Set
    End Property
    Public Property Phone As String
        Get
            Return _phone
        End Get
        Set(value As String)
            _phone = value
        End Set
    End Property
    Public Property Mobile As String
        Get
            Return _mobile
        End Get
        Set(ByVal value As String)
            _mobile = value
        End Set
    End Property
    Public Property Fax As String
        Get
            Return _fax
        End Get
        Set(ByVal value As String)
            _fax = value
        End Set
    End Property

End Class
