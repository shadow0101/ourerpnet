﻿Imports System.Security.AccessControl
Public NotInheritable Class SettingModel
    Private _path As String
    Public Property Path As String
        Get
            Return _path
        End Get
        Set(value As String)
            _path = value
        End Set
    End Property
    Public ReadOnly Property Windows() As String
        Get
            Return Environment.UserDomainName & "\" & Environment.UserName
        End Get
    End Property
End Class
Public MustInherit Class BaseModel
    Private _id As Integer
    Private _name As String
    Private _active As Boolean
    Private _createDate As Date
    Private _writeDate As Date
    Private _createUID As Integer
    Private _writeUID As Integer
    Public Property ID As Integer
        Get
            Return _id
        End Get
        Set(value As Integer)
            _id = value
        End Set
    End Property
    Public Property Name As String
        Get
            Return _name
        End Get
        Set(value As String)
            _name = value
        End Set
    End Property
    Public Property Active As Boolean
        Get
            Return _active
        End Get
        Set(value As Boolean)
            _active = value
        End Set
    End Property
    Public Property CreateDate As Date
        Get
            Return _createDate
        End Get
        Set(value As Date)
            _createDate = value
        End Set
    End Property
    Public Property WriteDate As Date
        Get
            Return _writeDate
        End Get
        Set(value As Date)
            _writeDate = value
        End Set
    End Property
    Public Property CreateUID As Integer
        Get
            Return _createUID
        End Get
        Set(value As Integer)
            _createUID = value
        End Set
    End Property
    Public Property WriteUID As Integer
        Get
            Return _writeUID
        End Get
        Set(value As Integer)
            _writeUID = value
        End Set
    End Property
End Class
