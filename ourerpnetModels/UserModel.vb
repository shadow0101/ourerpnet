﻿Public Class UserLogModel
    Inherits UserModel
    Private _action As String
    Private _userID As Integer
    Public Property Action As String
        Get
            Return _action
        End Get
        Set(value As String)
            _action = value
        End Set
    End Property
    Public Property UserID As Integer
        Get
            Return _userID
        End Get
        Set(value As Integer)
            _userID = value
        End Set
    End Property
End Class
Public Class UserModel
    Inherits BaseModel
    Private _login As String
    Private _password As String
    Private _passwordCrypt As String
    Private _isAdmin As Boolean
    Private _settings As Boolean
    Public Property Settings As Boolean
        Get
            Return _settings
        End Get
        Set(value As Boolean)
            _settings = value
        End Set
    End Property
    Public Property IsAdmin As Boolean
        Get
            Return _isAdmin
        End Get
        Set(value As Boolean)
            _isAdmin = value
        End Set
    End Property
    Public Property Login As String
        Get
            Return _login
        End Get
        Set(value As String)
            _login = value
        End Set
    End Property
    Public Property Password As String
        Get
            If IsNothing(_password) Then
                Return ""
            Else
                Return _password
            End If
        End Get
        Set(value As String)
            _password = value
        End Set
    End Property

    Public Property PasswordCrypt As String
        Get
            If IsNothing(_passwordCrypt) Then
                Return ""
            Else
                Return _passwordCrypt
            End If
        End Get
        Set(value As String)
            _passwordCrypt = value
        End Set
    End Property
End Class
