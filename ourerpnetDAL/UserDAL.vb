﻿Imports MySql.Data.MySqlClient
Imports ourerpnetModels.UserModel
Public Class UserDAL
    Inherits BaseDAL
    Private UsersModel As ourerpnetModels.UserModel
    Private UserLogModel As ourerpnetModels.UserLogModel
    Public Overloads Function write(ByVal objectUser As ourerpnetModels.UserModel) As Boolean
        Dim result As Boolean
        Using Connection = New MySqlConnection(ConnectionString)
            Connection.Open()
            Using Command = New MySqlCommand
                With Command
                    .Connection = Connection
                    .CommandText = "updateUser"
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.AddWithValue("@argID", objectUser.ID)
                    .Parameters("@argID").Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argName", objectUser.Name)
                    .Parameters("@argName").Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argActive", objectUser.Active)
                    .Parameters("@argActive").Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argLogin", objectUser.Login)
                    .Parameters("@argLogin").Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argPassword", objectUser.Password)
                    .Parameters("@argPassword").Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argPassword_crypt", objectUser.PasswordCrypt)
                    .Parameters("@argPassword_crypt").Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argWriteUID", objectUser.WriteUID)
                    .Parameters("@argWriteUID").Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argIs_admin", objectUser.IsAdmin)
                    .Parameters("@argIs_admin").Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argSettings", objectUser.Settings)
                    .Parameters("@argSettings").Direction = ParameterDirection.Input
                    .Parameters.Add("@result", MySqlDbType.Int32)
                    .Parameters("@result").Direction = ParameterDirection.Output
                    .ExecuteNonQuery()
                    Dim id As Integer = .Parameters("@result").Value
                    If id > 0 Then
                        result = True
                    End If
                End With
            End Using
        End Using
        Return result
    End Function
    Public Overloads Function BrowseID(ByVal id As Integer) As ourerpnetModels.UserModel
        Using Connection As New MySqlConnection(ConnectionString)
            Using Command As New MySqlCommand("browse", Connection)
                With Command
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.AddWithValue("argTable", "users")
                    .Parameters.AddWithValue("argID", id)
                End With
                Connection.Open()
                Dim Reader As MySqlDataReader
                Reader = Command.ExecuteReader()
                UsersModel = New ourerpnetModels.UserModel
                While Reader.Read()
                    UsersModel.ID = Reader.GetInt32(0)
                    UsersModel.Name = Reader.GetString(1)
                    UsersModel.Active = Reader.GetInt16(2)
                    UsersModel.Login = Reader.GetString(3)
                    UsersModel.PasswordCrypt = Reader.GetString(4)
                    UsersModel.IsAdmin = Reader.GetBoolean(5)
                    UsersModel.Settings = Reader.GetBoolean(6)
                End While
                If Reader.HasRows = False Then
                    UsersModel = Nothing
                End If
                Reader.Close()
            End Using
        End Using
        Return UsersModel
    End Function
    Public Function CheckUser(ByVal login As String, ByVal password_crypt As String) As ourerpnetModels.UserModel
        Using Connection As New MySqlConnection(ConnectionString)
            Dim query As String = _
                "SELECT u.id,u.login,u.password_crypt,ur.is_admin,ur.settings FROM users u INNER JOIN user_rights ur ON u.id = ur.user_id WHERE u.login=@login AND u.password_crypt=@password_crypt"
            Using Command As New MySqlCommand(query, Connection)
                Connection.Open()
                With Command
                    .CommandType = CommandType.Text
                    .Prepare()
                    .Parameters.AddWithValue("@login", login)
                    .Parameters.AddWithValue("@password_crypt", password_crypt)
                End With

                Dim Reader As MySqlDataReader
                Reader = Command.ExecuteReader()
                UsersModel = New ourerpnetModels.UserModel
                While Reader.Read()
                    UsersModel.ID = Reader.GetInt32(0)
                    UsersModel.Login = Reader.GetString(1)
                    UsersModel.PasswordCrypt = Reader.GetString(2)
                    UsersModel.IsAdmin = Reader.GetBoolean(3)
                    UsersModel.Settings = Reader.GetBoolean(4)
                End While
                Reader.Close()
            End Using
        End Using
        Return UsersModel
    End Function
    Public Overloads Function all() As List(Of ourerpnetModels.UserModel)
        Dim UsersList As New List(Of ourerpnetModels.UserModel)
        'Dim par As New MySqlParameter("@argTable", MySqlDbType.VarChar)
        'par.Value = "users"
        Dim Reader As MySqlDataReader
        Using Connection As New MySqlConnection(ConnectionString)
            Using Command As New MySqlCommand("all", Connection)
                With Command
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.AddWithValue("@argTable", "users")
                End With
                Connection.Open()
                Reader = Command.ExecuteReader()
                Dim user As ourerpnetModels.UserModel
                'select u.id,u.name,u.active,u.login,u.password_crypt,ur.is_admin,ur.settings from users u inner join user_rights ur on u.id=ur.user_id;
                While Reader.Read()
                    user = New ourerpnetModels.UserModel
                    user.ID = Reader.GetInt32(0)
                    user.Name = Reader.GetString(1)
                    user.Active = Reader.GetBoolean(2)
                    user.Login = Reader.GetString(3)
                    user.PasswordCrypt = Reader.GetString(4)
                    user.IsAdmin = Reader.GetBoolean(5)
                    user.Settings = Reader.GetBoolean(6)
                    UsersList.Add(user)
                End While
                Reader.Close()
            End Using
        End Using
        Return UsersList
    End Function
    Public Overloads Function create(ByVal objectUser As ourerpnetModels.UserModel) As Boolean
        Dim result As Boolean
        Using Connection = New MySqlConnection(ConnectionString)
            Connection.Open()
            Using Command = New MySqlCommand
                With Command
                    .Connection = Connection
                    .CommandText = "createUser"
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.AddWithValue("@argName", objectUser.Name)
                    .Parameters("@argName").Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argActive", objectUser.Active)
                    .Parameters("@argActive").Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argLogin", objectUser.Login)
                    .Parameters("@argLogin").Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argPassword", objectUser.Password)
                    .Parameters("@argPassword").Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argPassword_crypt", objectUser.PasswordCrypt)
                    .Parameters("@argPassword_crypt").Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argCreateUID", objectUser.CreateUID)
                    .Parameters("@argCreateUID").Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argIs_admin", objectUser.IsAdmin)
                    .Parameters("@argIs_admin").Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argSettings", objectUser.Settings)
                    .Parameters("@argSettings").Direction = ParameterDirection.Input
                    .Parameters.Add("@result", MySqlDbType.Int32).Direction = ParameterDirection.Output
                    .ExecuteNonQuery()
                    Dim id As Integer = .Parameters("@result").Value
                    If id > 0 Then
                        result = True
                    End If
                End With
            End Using
        End Using
        Return result
    End Function
    Public Overloads Function all(ByVal table As String) As List(Of ourerpnetModels.UserLogModel)
        Dim UserLogs As New List(Of ourerpnetModels.UserLogModel)
        Dim Reader As MySqlDataReader
        Using Connection As New MySqlConnection(ConnectionString)
            Using Command As New MySqlCommand("all", Connection)
                With Command
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.AddWithValue("@argTable", "users_log")
                End With
                Connection.Open()
                Reader = Command.ExecuteReader()
                Dim user As ourerpnetModels.UserLogModel
                '    select ul.id,ul.user_id,ul.action,ul.date,u.name from users_log ul
                ' left join users u on ul.user_id = u.id;
                While Reader.Read()
                    user = New ourerpnetModels.UserLogModel
                    user.ID = Reader.GetInt32(0)
                    user.UserID = Reader.GetInt32(1)
                    user.Action = Reader.GetString(2)
                    user.CreateDate = Reader.GetDateTime(3)
                    user.Name = Reader.GetString(4)
                    UserLogs.Add(user)
                End While
                Reader.Close()
            End Using
        End Using
        Return UserLogs
    End Function
    Public Function browseLogs(ByVal ID As Integer) As List(Of ourerpnetModels.UserLogModel)
        Dim logList As New List(Of ourerpnetModels.UserLogModel)
        Using Connection As New MySqlConnection(ConnectionString)
            Using Command As New MySqlCommand("browse", Connection)
                With Command
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.AddWithValue("argTable", "users_log")
                    .Parameters.AddWithValue("argID", ID)
                End With
                Connection.Open()
                Dim Reader As MySqlDataReader
                '		select ul.id,ul.user_id,ul.action,ul.date,u.name from users_log ul
                '   inner join users u on ul.user_id = u.id where u.id=argID;
                Reader = Command.ExecuteReader()
                While Reader.Read()
                    UserLogModel = New ourerpnetModels.UserLogModel
                    UserLogModel.ID = Reader.GetInt32(0)
                    UserLogModel.UserID = Reader.GetInt32(1)
                    UserLogModel.Action = Reader.GetString(2)
                    UserLogModel.CreateDate = Reader.GetDateTime(3)
                    UserLogModel.Name = Reader.GetString(4)
                    logList.Add(UserLogModel)
                End While
                Reader.Close()
            End Using
        End Using
        Return logList
    End Function
End Class