﻿Imports MySql.Data.MySqlClient
Public Class PartnerDAL
    Inherits BaseDAL
    Private PModel As ourerpnetModels.PartnerModel
    Private part As ourerpnetModels.PartnerModel

    Public Overloads Function write(ByVal oPartner As ourerpnetModels.PartnerModel) As Boolean
        Dim result As Boolean
        Using Connection = New MySqlConnection(ConnectionString)
            Connection.Open()
            Using Command = New MySqlCommand
                ' update partner set name=argName,active=argActive,street=argStreet,city=argCity,zip=argZip,
                '  email=argEmail,job_position=argJobPosition,phone=argPhone,mobile=argMobile,fax=argFax,writeUID=argWriteUID where id=argID;
                With Command
                    .Connection = Connection
                    .CommandText = "partner"
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.AddWithValue("@argAction", "write").Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argID", oPartner.ID).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argName", oPartner.Name).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argActive", oPartner.Active).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argStreet", oPartner.Street).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argCity", oPartner.City).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argZip", oPartner.Zip).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argEmail", oPartner.Email).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argJobPosition", oPartner.JobPosition).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argPhone", oPartner.Phone).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argMobile", oPartner.Mobile).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argFax", oPartner.Fax).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argCreateUID", Nothing).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argWriteUID", oPartner.WriteUID).Direction = ParameterDirection.Input
                    .Parameters.Add("@result", MySqlDbType.Int32).Direction = ParameterDirection.Output
                    .ExecuteNonQuery()
                    Dim id As Integer = .Parameters("@result").Value
                    If id > 0 Then
                        result = True
                    End If
                End With
            End Using
        End Using
        Return result
    End Function
    Public Overloads Function all() As List(Of ourerpnetModels.PartnerModel)
        Dim PartnerList As New List(Of ourerpnetModels.PartnerModel)
        'Dim par As New MySqlParameter("@argTable", MySqlDbType.VarChar)
        'par.Value = "users"
        Dim Reader As MySqlDataReader
        Using Connection As New MySqlConnection(ConnectionString)
            Using Command As New MySqlCommand("all", Connection)
                With Command
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.AddWithValue("@argTable", "partner")
                End With
                Connection.Open()
                Reader = Command.ExecuteReader()
                '   p.id,p.name,p.active,p.street,p.city,p.zip,p.email,
                '   p.job_position,p.phone,p.mobile,p.fax from partner p;
                While Reader.Read()
                    part = New ourerpnetModels.PartnerModel
                    part.ID = Reader.GetInt32(0)
                    part.Name = Reader.GetString(1)
                    part.Active = Reader.GetBoolean(2)
                    part.Street = Reader.GetString(3)
                    part.City = Reader.GetString(4)
                    part.Zip = Reader.GetString(5)
                    part.Email = Reader.GetString(6)
                    part.JobPosition = Reader.GetString(7)
                    part.Phone = Reader.GetString(8)
                    part.Mobile = Reader.GetString(9)
                    part.Fax = Reader.GetString(10)
                    PartnerList.Add(part)
                End While
                Reader.Close()
            End Using
        End Using
        Return PartnerList
    End Function
    Public Overloads Function create(ByVal oPartner As ourerpnetModels.PartnerModel) As Boolean
        Dim result As Boolean
        Using Connection = New MySqlConnection(ConnectionString)
            Connection.Open()
            Using Command = New MySqlCommand
                With Command
                    'IN argName varchar(64),IN argActive tinyint(1),IN argStreet varchar(64),IN argCity varchar(64),IN argZip varchar(64),IN argEmail varchar(64),
                    'IN argJobPosition varchar(64),IN argPhone varchar(64),IN argMobile varchar(64),IN argFax varchar(64),IN argCreateUID int(11),
                    'IN argWriteUID int(11),IN argAction varchar(32),argID int(11),OUT result int(11)
                    .Connection = Connection
                    .CommandText = "partner"
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.AddWithValue("@argName", oPartner.Name).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argActive", oPartner.Active).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argStreet", oPartner.Street).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argCity", oPartner.City).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argZip", oPartner.Zip).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argEmail", oPartner.Email).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argJobPosition", oPartner.JobPosition).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argPhone", oPartner.Phone).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argMobile", oPartner.Mobile).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argFax", oPartner.Fax).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argCreateUID", oPartner.CreateUID).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argWriteUID", oPartner.WriteUID).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argAction", "create").Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argID", Nothing).Direction = ParameterDirection.Input
                    .Parameters.Add("@result", MySqlDbType.Int32).Direction = ParameterDirection.Output
                    .ExecuteNonQuery()
                    Dim id As Integer = .Parameters("@result").Value
                    If id > 0 Then
                        result = True
                    End If
                End With
            End Using
        End Using
        Return result
    End Function
End Class
