﻿Imports MySql.Data.MySqlClient
Public Class BaseDAL
    'Protected Connection As MySqlConnection
    'Protected Command As MySqlCommand
    'Private Reader As MySqlDataReader
    Private Smodel As ourerpnetModels.SettingModel
    Protected Reader As MySqlDataReader
    Private _connectionString As String = "Data Source=LocalHost;database=ourerpnet;user=root;password="
    Public Sub New() 'Constructor for Connection string For 'ourerpnet' Database 
        'ConnectionString = 
    End Sub
    Public Function write(ByVal path As String) As Boolean
        Dim result As Boolean = False
        Using Connection = New MySqlConnection(ConnectionString)
            Connection.Open()
            Using Command = New MySqlCommand
                ' update partner set name=argName,active=argActive,street=argStreet,city=argCity,zip=argZip,
                '  email=argEmail,job_position=argJobPosition,phone=argPhone,mobile=argMobile,fax=argFax,writeUID=argWriteUID where id=argID;
                With Command
                    .Connection = Connection
                    .CommandText = "settings"
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.AddWithValue("@argPath", path).Direction = ParameterDirection.Input
                    .Parameters.Add("@result", MySqlDbType.Int32).Direction = ParameterDirection.Output
                    .ExecuteNonQuery()
                    Dim id As Integer = .Parameters("@result").Value
                    If id > 0 Then
                        result = True
                    End If
                End With
            End Using
        End Using
        Return result
    End Function
    Public Function getSettings() As ourerpnetModels.SettingModel
        Using Connection As New MySqlConnection(ConnectionString)
            Using Command As New MySqlCommand("all", Connection)
                With Command
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.AddWithValue("argTable", "base_settings")
                End With
                Connection.Open()
                Reader = Command.ExecuteReader()
                Smodel = New ourerpnetModels.SettingModel
                While Reader.Read()
                    Smodel.Path = Reader.GetString(0)
                End While
                Reader.Close()
            End Using
        End Using
        Return Smodel
    End Function
    Public Property ConnectionString As String
        Get
            Return _connectionString
        End Get
        Set(value As String)
            _connectionString = value
        End Set
    End Property
    Public Function all(ByVal par As MySqlParameter) As MySqlDataReader
        Dim result, res As MySqlDataReader
        Using Connection As New MySqlConnection(ConnectionString)
            Using Command As New MySqlCommand("all", Connection)
                With Command
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add(par)
                End With
                Connection.Open()
                result = Command.ExecuteReader()
            End Using
        End Using
        res = result
        Return res
    End Function
    Public Overridable Function delete(ByVal table As String, ByVal id As Integer) As Boolean
        Dim result As Boolean
        Using Connection As New MySqlConnection(ConnectionString)
            Using Command As New MySqlCommand("delete", Connection)
                With Command
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.AddWithValue("argTable", table).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("argID", id).Direction = ParameterDirection.Input
                    .Parameters.Add("@result", MySqlDbType.Int32).Direction = ParameterDirection.Output
                End With
                Connection.Open()
                Command.ExecuteNonQuery()
                Dim res As Integer = Command.Parameters("@result").Value
                If res > 0 Then
                    result = True
                End If
            End Using
        End Using
        Return result
    End Function


    'Public Overridable Function Browse(ByVal tablename As String, id As Integer)
    '    Using Connection As New MySqlConnection(ConnectionString)
    '        Connection.Open()
    '        Using Command As New MySqlCommand
    '            With Command
    '                .Connection = Connection
    '                .CommandText = "browse"
    '                .CommandType = CommandType.StoredProcedure
    '                .Parameters.AddWithValue("@tableName", tablename)
    '                .Parameters("@tableName").Direction = ParameterDirection.Input
    '                .Parameters.AddWithValue("@parID", id)
    '                .Parameters("@parID").Direction = ParameterDirection.Input
    '            End With
    '        End Using
    '    End Using
    'End Function
End Class
