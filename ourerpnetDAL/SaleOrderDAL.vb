﻿Imports MySql.Data.MySqlClient
Public Class SaleOrderDAL
    Inherits BaseDAL
    Public Sub backup()
        Using Connection = New MySqlConnection(ConnectionString)
            Connection.Open()
            Using Command = New MySqlCommand
                With Command
                    .Connection = Connection
                    .CommandText = "backup"
                    .CommandType = CommandType.StoredProcedure
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Public Overloads Function create(ByVal oSO As ourerpnetModels.SaleOrderModel) As Boolean
        Dim result As Boolean = False
        Dim OrderID As Integer
        Dim Transact As MySqlTransaction
        Dim name As String = "SO"
        Using Connection = New MySqlConnection(ConnectionString)
            Connection.Open()
            Transact = Connection.BeginTransaction()
            Try
                Using Command = New MySqlCommand
                    With Command
                        .Connection = Connection
                        .Transaction = Transact
                        .CommandText = "INSERT INTO sale_order(partner_id,date_order,amount_total,user_id,createUID,writeUID,createDate,writeDate,status)" & _
                            "values(@partnerId,@dateOrder,@amountTotal,@userID,@createUID,@writeUID,current_timestamp(),current_timestamp(),@status)"
                        .Parameters.AddWithValue("@partnerID", oSO.PartnerID)
                        .Parameters.AddWithValue("@dateOrder", oSO.DateOrder)
                        .Parameters.AddWithValue("@amountTotal", oSO.AmountTotal)
                        .Parameters.AddWithValue("@userID", oSO.UserID)
                        .Parameters.AddWithValue("@createUID", oSO.UserID) '???
                        .Parameters.AddWithValue("@writeUID", oSO.UserID) '???
                        .Parameters.AddWithValue("@status", oSO.Status)
                        .ExecuteNonQuery()

                        OrderID = .LastInsertedId
                        name &= OrderID.ToString
                        .CommandText = "Update sale_order set name=@argName where id=@OrderID"
                        .Parameters.AddWithValue("@argName", name)
                        .Parameters.AddWithValue("@OrderID", OrderID)
                        .ExecuteNonQuery()
                        .Parameters.Clear()
                        .CommandText = "INSERT INTO sale_order_line(order_id,price_unit,quantity,product_id,price_subtotal,price_tax)" & _
                              "Values(" & OrderID & ",@priceUnit,@quantity,@productID,@priceSubtotal,@priceTax)"
                        .Prepare()
                        .Parameters.Add("@priceUnit", MySqlDbType.Decimal)
                        .Parameters.Add("@quantity", MySqlDbType.Int32)
                        .Parameters.Add("@productID", MySqlDbType.Int32)
                        .Parameters.Add("@priceSubtotal", MySqlDbType.Decimal)
                        .Parameters.Add("@priceTax", MySqlDbType.Decimal)
                        For Each line As ourerpnetModels.OrderLineModel In oSO.OrderLine
                            .Parameters("@priceUnit").Value = line.PriceUnit
                            .Parameters("@quantity").Value = line.Quantity
                            .Parameters("@productID").Value = line.ProductID
                            .Parameters("@priceSubtotal").Value = line.PriceSubtotal
                            .Parameters("@priceTax").Value = line.PriceTax
                            .ExecuteNonQuery()
                        Next
                        Transact.Commit()
                        result = True
                    End With
                End Using
            Catch ex As MySqlException
                Transact.Rollback()
                Throw New Exception(ex.Message)
            End Try
        End Using
        Return result
    End Function
    Public Overloads Function all() As List(Of ourerpnetModels.SaleOrderModel)
        Dim SaleOrderList As New List(Of ourerpnetModels.SaleOrderModel)
        Dim Reader As MySqlDataReader
        Using Connection As New MySqlConnection(ConnectionString)
            Using Command As New MySqlCommand("all", Connection)
                With Command
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.AddWithValue("@argTable", "sale_order")
                End With
                Connection.Open()
                Reader = Command.ExecuteReader()
                Dim sale_order As ourerpnetModels.SaleOrderModel
                'so.id, so.name, so.date_order, so.partner_id, so.amount_total, so.status
                While Reader.Read()
                    sale_order = New ourerpnetModels.SaleOrderModel
                    sale_order.ID = Reader.GetInt32(0)
                    sale_order.Name = Reader.GetString(1)
                    sale_order.DateOrder = Reader.GetMySqlDateTime(2)
                    sale_order.PartnerID = Reader.GetInt32(3)
                    sale_order.AmountTotal = Reader.GetDecimal(4)
                    sale_order.Status = Reader.GetString(5)
                    SaleOrderList.Add(sale_order)
                End While
                Reader.Close()
            End Using
        End Using
        Return SaleOrderList
    End Function
End Class
