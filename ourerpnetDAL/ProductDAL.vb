﻿Imports MySql.Data.MySqlClient
Imports ourerpnetModels
Public Class ProductDAL
    Inherits BaseDAL
    Private PModel As ourerpnetModels.ProductModel
    Public Overloads Function BrowseID(ByVal id As Integer) As ourerpnetModels.ProductModel
        Using Connection As New MySqlConnection(ConnectionString)
            Using Command As New MySqlCommand("browse", Connection)
                With Command
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.AddWithValue("argTable", "product")
                    .Parameters.AddWithValue("argID", id)
                End With
                Connection.Open()
                Reader = Command.ExecuteReader()
                PModel = New ourerpnetModels.ProductModel

                While Reader.Read()
                    PModel.ID = Reader.GetInt32(0)
                    PModel.Name = Reader.GetString(1)
                    PModel.Description = Reader.GetString(2)
                    PModel.Active = Reader.GetInt16(3)
                    PModel.Type = Reader.GetString(4)
                    PModel.Barcode = Reader.GetString(5)
                    PModel.Cost = Reader.GetDecimal(6)
                    PModel.SalePrice = Reader.GetDecimal(7)
                    PModel.Weight = Reader.GetDecimal(8)
                    PModel.Volume = Reader.GetDecimal(9)
                    PModel.CategoryName = Reader.GetString(10)
                    PModel.CategoryID = Reader.GetInt32(11)
                    PModel.TaxID = If(Reader.IsDBNull(12), Nothing, Reader.GetInt32(12))
                    PModel.LocationID = If(Reader.IsDBNull(14), Nothing, Reader.GetInt32(14))
                    PModel.Quantity = If(Reader.IsDBNull(15), Nothing, Reader.GetInt32(15))
                    PModel.TaxAmount = If(Reader.IsDBNull(16), Nothing, Reader.GetDecimal(16))
                End While
                If Reader.HasRows = False Then
                    PModel = Nothing
                End If
                Reader.Close()
            End Using
        End Using
        Return PModel
    End Function
    Public Overloads Function write(ByVal oProduct As ourerpnetModels.ProductModel) As Boolean
        Dim result As Boolean = False
        Using Connection = New MySqlConnection(ConnectionString)
            Connection.Open()
            Using Command = New MySqlCommand
                ' update partner set name=argName,active=argActive,street=argStreet,city=argCity,zip=argZip,
                '  email=argEmail,job_position=argJobPosition,phone=argPhone,mobile=argMobile,fax=argFax,writeUID=argWriteUID where id=argID;
                With Command
                    .Connection = Connection
                    .CommandText = "product"
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.AddWithValue("@argQuantity", oProduct.Quantity).Direction = ParameterDirection.Input

                    .Parameters.AddWithValue("@argAction", "write").Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argID", oProduct.ID).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argName", oProduct.Name).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argActive", oProduct.Active).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argDescription", oProduct.Description).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argType", oProduct.Type).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argBarcode", oProduct.Barcode).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argCost", oProduct.Cost).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argSale_price", oProduct.SalePrice).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argWeight", oProduct.Weight).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argVolume", oProduct.Volume).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argCategory_id", oProduct.CategoryID).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argTax_id", oProduct.TaxID).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argLocation_id", oProduct.LocationID).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argWriteUID", oProduct.WriteUID).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argCreateUID", Nothing).Direction = ParameterDirection.Input
                    .Parameters.Add("@result", MySqlDbType.Int32).Direction = ParameterDirection.Output
                    .ExecuteNonQuery()
                    Dim id As Integer = .Parameters("@result").Value
                    If id > 0 Then
                        result = True
                    End If
                End With
            End Using
        End Using
        Return result
    End Function
    Public Overloads Function all() As List(Of ourerpnetModels.ProductModel)
        Dim ProductList As New List(Of ourerpnetModels.ProductModel)
        Using Connection As New MySqlConnection(ConnectionString)
            Using Command As New MySqlCommand("all", Connection)
                With Command
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.AddWithValue("@argTable", "product")
                End With
                Connection.Open()
                Reader = Command.ExecuteReader()
                '        select p.id,p.name as PNAME,p.description,p.active,p.type,p.barcode,p.cost,p.sale_price,
                'p.weight,p.volume,c.name as CNAME,p.category_id,p.tax_id,ac.name as TNAME,p.location_id,sp.quantity
                While Reader.Read()
                    PModel = New ourerpnetModels.ProductModel
                    PModel.ID = Reader.GetInt32(0)
                    PModel.Name = Reader.GetString(1)
                    PModel.Description = Reader.GetString(2)
                    PModel.Active = Reader.GetInt16(3)
                    PModel.Type = Reader.GetString(4)
                    PModel.Barcode = Reader.GetString(5)
                    PModel.Cost = Reader.GetDecimal(6)
                    PModel.SalePrice = Reader.GetDecimal(7)
                    PModel.Weight = Reader.GetDecimal(8)
                    PModel.Volume = Reader.GetDecimal(9)
                    PModel.CategoryName = Reader.GetString(10)
                    PModel.CategoryID = Reader.GetInt32(11)
                    PModel.TaxID = If(Reader.IsDBNull(12), Nothing, Reader.GetInt32(12))
                    PModel.LocationID = If(Reader.IsDBNull(14), Nothing, Reader.GetInt32(14))
                    PModel.Quantity = If(Reader.IsDBNull(15), Nothing, Reader.GetInt32(15))
                    ProductList.Add(PModel)
                End While
                Reader.Close()
            End Using
        End Using
        Return ProductList
    End Function
    Public Overloads Function create(ByVal oProduct As ourerpnetModels.ProductModel) As Boolean
        Dim result As Boolean
        Using Connection = New MySqlConnection(ConnectionString)
            Connection.Open()
            Using Command = New MySqlCommand
                With Command
                    'IN argName varchar(64),IN argActive tinyint(1),IN argDescription varchar(255),IN argType enum('consume','service','product'),
                    'IN argBarcode varchar(255),IN argCost decimal,IN argSale_price decimal,IN argWeight decimal,IN argVolume decimal,
                    'IN argCategory_id int(11),IN argTax_id int(11),IN argLocation_id int(11),IN argCreateUID int(11),IN argWriteUID int(11),
                    'IN argAction varchar(32),argID int(11),OUT result int(11)
                    .Connection = Connection
                    .CommandText = "product"
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.AddWithValue("@argQuantity", oProduct.Quantity).Direction = ParameterDirection.Input

                    .Parameters.AddWithValue("@argName", oProduct.Name).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argActive", oProduct.Active).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argDescription", oProduct.Description).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argType", oProduct.Type).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argBarcode", oProduct.Barcode).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argCost", oProduct.Cost).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argSale_price", oProduct.SalePrice).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argWeight", oProduct.Weight).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argVolume", oProduct.Volume).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argCategory_id", oProduct.CategoryID).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argTax_id", oProduct.TaxID).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argLocation_id", oProduct.LocationID).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argCreateUID", oProduct.CreateUID).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argWriteUID", oProduct.WriteUID).Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argAction", "create").Direction = ParameterDirection.Input
                    .Parameters.AddWithValue("@argID", Nothing).Direction = ParameterDirection.Input
                    .Parameters.Add("@result", MySqlDbType.Int32).Direction = ParameterDirection.Output
                    .ExecuteNonQuery()
                    Dim id As Integer = .Parameters("@result").Value
                    If id > 0 Then
                        result = True
                    End If
                End With
            End Using
        End Using
        Return result
    End Function
    Public Overloads Function create(ByVal table As String, ByVal oProduct As ourerpnetModels.ProductModel) As Boolean
        Dim result As Boolean
        Using Connection = New MySqlConnection(ConnectionString)
            Connection.Open()
            Using Command = New MySqlCommand
                With Command
                    .Connection = Connection
                    If table = "stock_location" Then
                        .CommandText = "stock_location"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.AddWithValue("@argName", oProduct.Name).Direction = ParameterDirection.Input
                        .Parameters.AddWithValue("@argActive", oProduct.Active).Direction = ParameterDirection.Input
                        .Parameters.AddWithValue("@argCreateUID", oProduct.CreateUID).Direction = ParameterDirection.Input
                        .Parameters.AddWithValue("@argWriteUID", oProduct.WriteUID).Direction = ParameterDirection.Input
                        .Parameters.AddWithValue("@argAction", "create").Direction = ParameterDirection.Input
                        .Parameters.AddWithValue("@argID", Nothing).Direction = ParameterDirection.Input
                        .Parameters.Add("@result", MySqlDbType.Int32).Direction = ParameterDirection.Output
                        .ExecuteNonQuery()
                        Dim id As Integer = .Parameters("@result").Value
                        If id > 0 Then
                            result = True
                        End If
                    ElseIf table = "product_category" Then
                        .CommandText = "product_category"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.AddWithValue("@argName", oProduct.Name).Direction = ParameterDirection.Input
                        .Parameters.AddWithValue("@argActive", oProduct.Active).Direction = ParameterDirection.Input
                        .Parameters.AddWithValue("@argCreateUID", oProduct.CreateUID).Direction = ParameterDirection.Input
                        .Parameters.AddWithValue("@argWriteUID", oProduct.WriteUID).Direction = ParameterDirection.Input
                        .Parameters.AddWithValue("@argAction", "create").Direction = ParameterDirection.Input
                        .Parameters.AddWithValue("@argID", Nothing).Direction = ParameterDirection.Input
                        .Parameters.Add("@result", MySqlDbType.Int32).Direction = ParameterDirection.Output
                        .ExecuteNonQuery()
                        Dim id As Integer = .Parameters("@result").Value
                        If id > 0 Then
                            result = True
                        End If
                    ElseIf table = "account_tax" Then
                        .CommandText = "account_tax"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.AddWithValue("@argName", oProduct.Name).Direction = ParameterDirection.Input
                        .Parameters.AddWithValue("@argAmount", oProduct.TaxAmount).Direction = ParameterDirection.Input
                        .Parameters.AddWithValue("@argCreateUID", oProduct.CreateUID).Direction = ParameterDirection.Input
                        .Parameters.AddWithValue("@argWriteUID", oProduct.WriteUID).Direction = ParameterDirection.Input
                        .Parameters.AddWithValue("@argAction", "create").Direction = ParameterDirection.Input
                        .Parameters.AddWithValue("@argID", Nothing).Direction = ParameterDirection.Input
                        .Parameters.Add("@result", MySqlDbType.Int32).Direction = ParameterDirection.Output
                        .ExecuteNonQuery()
                        Dim id As Integer = .Parameters("@result").Value
                        If id > 0 Then
                            result = True
                        End If
                    End If
                End With
            End Using
        End Using
        Return result
    End Function
    Public Function getTable(ByVal table As String) As Dictionary(Of Integer, String)
        Dim tableDict As Dictionary(Of Integer, String)
        Using Connection As New MySqlConnection(ConnectionString)
            Using Command As New MySqlCommand("all", Connection)
                With Command
                    .CommandType = CommandType.StoredProcedure
                    If table = "product_category" Then
                        .Parameters.AddWithValue("@argTable", "product_category")
                    ElseIf table = "stock_location" Then
                        .Parameters.AddWithValue("@argTable", "stock_location")
                    ElseIf table = "account_tax" Then
                        .Parameters.AddWithValue("@argTable", "account_tax")
                    End If
                End With
                Connection.Open()
                Reader = Command.ExecuteReader()
                tableDict = New Dictionary(Of Integer, String)
                While Reader.Read()
                    tableDict.Add(Reader.GetInt32(0), Reader.GetString(1))
                End While
                Reader.Close()
            End Using
        End Using
        Return tableDict
    End Function
    Public Overloads Function productForeign(ByVal table As String) As List(Of ProductModel)
        Dim ProductList As New List(Of ourerpnetModels.ProductModel)
        Using Connection As New MySqlConnection(ConnectionString)
            Using Command As New MySqlCommand("all", Connection)
                Connection.Open()
                Command.CommandType = CommandType.StoredProcedure
                If table = "product_category" Then
                    Command.Parameters.AddWithValue("@argTable", "product_category")
                    Reader = Command.ExecuteReader()
                    While Reader.Read()
                        PModel = New ourerpnetModels.ProductModel
                        PModel.ID = Reader.GetInt32(0)
                        PModel.Name = Reader.GetString(1)
                        ProductList.Add(PModel)
                    End While
                ElseIf table = "stock_location" Then
                    Command.Parameters.AddWithValue("@argTable", "stock_location")
                    Reader = Command.ExecuteReader()
                    While Reader.Read()
                        PModel = New ourerpnetModels.ProductModel
                        PModel.ID = Reader.GetInt32(0)
                        PModel.Name = Reader.GetString(1)
                        ProductList.Add(PModel)
                    End While
                ElseIf table = "account_tax" Then
                    Command.Parameters.AddWithValue("@argTable", "account_tax")
                    Reader = Command.ExecuteReader()
                    While Reader.Read()
                        PModel = New ourerpnetModels.ProductModel
                        PModel.ID = Reader.GetInt32(0)
                        PModel.Name = Reader.GetString(1)
                        ProductList.Add(PModel)
                    End While
                End If
                Reader.Close()
            End Using
        End Using
        Return ProductList
    End Function
End Class
